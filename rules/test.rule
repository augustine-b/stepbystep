#ruleset test

[ ZerothPowerOfZero ]
0^0
  :: undefined
;

[ NonIntegerRoot ]
nroot(%0, %1)
  %1 is not integer
  :: undefined
;

[ ZerothPower ]
%0^0
  %0 != 0
  :: 1
;

[ PowerOfZero ]
0^%0
  %0 != 0
  :: 0
;

[ EvalPower ]
%0^%1
  %0 is integer
  %1 is integer
  :: eval(%0 ^ %1)
;

[ RootOfOne ]
nroot(1, %0)
  %0 is integer
  :: 1
;

[ ZeroInSum ]
@persistent
0 + %%
  :: %%
;

[ OneInProduct ]
@persistent
1 * %%
  :: %%
;

[ IntegerFraction ]
%0/%1
  %0 is integer
  %1 is integer

  $div := gcd(%0, %1)
  $div != 1

  $a := eval(%0 / $div)

  $div == %1 -> {
    :: $a
  }

  $b := eval(%1 / $div)
  :: $a / $b
;

[ SameNumDenom ]
%0/%0
  :: 1
;

[ OneDenom ]
%0/1
  :: %0
;

[ NegativeFraction ]
(-%0)/%1
  :: -(%0/%1)
;

[ AddNumeric ]
%0 + %1 + %%
  %0 is numeric
  %1 is numeric
  $sum := eval(%0 + %1)
  :: $sum + %%
;

[ MultiplyNumeric ]
%0 * %1 * %%
  %0 is numeric
  %1 is numeric
  $product := eval(%0 * %1)
  :: $product * %%
;

[ FactorInteger ]
%0 + %1 + %%
  $int1, $rest1 := split_integer(%0)
  $int2, $rest2 := split_integer(%1)

  $rest1 == $rest2

  :: ($int1 + $int2) * $rest1 + %%
;

[ DistributeRootOverFraction ]
sqrt(%0 / %1)
  :: sqrt(%0) / sqrt(%1)
;

[ EvaluateIntegerSquareRoot ]
sqrt(%0)
  %0 is numeric
  $root := eval(sqrt(%0))
  $root is integer
  :: $root
;

[ ArrangeLinearConstant ]
%0 + %% = %1
  %0 is not constant
  %% is constant
  %1 is constant
  :: %0 = %1 - %%
;

[ ArrangeLinearVariable ]
%0 = %1 + %%
  %0 is not constant
  %1 is not constant
  %% is constant
  :: %0 - %1 = %%
;

[ DivideLinear ]
%0 * %1 = %2
  %0 is not constant
  %1 is constant
  %2 is constant
  :: %0 = %2 / %1
;

[ ReorganizeQuadratic ]
@skipif ReorganizeQuadratic
x^2 + %0 * x + %1 = %2
  %0 is constant
  %1 is constant
  %2 is constant

  :: x^2 + %0 * x + %1 = %2
;

[ BinomSquared ]
x^2 + %0 * x + %1 = %2
  %0 is even
  %1 is integer
  %2 is constant

  $a := eval(%0 / 2)
  $tmp := eval(sqrt(%1))
  $a == $tmp

  :: (x + $a)^2 = %2
;

[ TakeSquareRootZero ]
(%0)^2 = 0
  %0 is not constant
  :: %0 = 0
;

[ TakeSquareRoot ]
(%0)^2 = %1
  %0 is not constant
  %1 is constant

  %1 < 0 -> :: undefined on R

  :: { %0 = sqrt(%1), %0 = -sqrt(%1) }
;

[ CompleteSquare ]
x^2 + %0 * x + %1 = %2
  %0 is even
  %1 is constant
  %2 is constant

  $a := eval(%0 / 2)
  $add := eval($a^2 - %1)
  $tmp := $add

  :: x^2 + %0 * x + %1 + $add = %2 + $tmp
;

[ SolveQuadratic ]
%0 * x^2 + %1 * x + %2 = 0
  $delta := %1^2 - 4 * %0 * %2
  :: x = (-%1 (+-) sqrt($delta)) / (2 * %0)
;
