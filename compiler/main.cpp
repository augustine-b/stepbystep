#include <fstream>
#include <fstream>
#include "parser/Driver.h"
#include "script/Ruleset.h"

int main ()
{
    Driver drv;

    std::vector<Heuristic> heuristics;
    heuristics.push_back(drv.parse_heuristic("../heuristics/sgn.heuristic"));
    heuristics.push_back(drv.parse_heuristic("../heuristics/compare.heuristic"));

    for (const Heuristic& heuristic : heuristics) {
        std::ofstream hheader("../compiled/" + heuristic.name + "_heuristic.hpp");
        std::ofstream hsource("../compiled/" + heuristic.name + "_heuristic.cpp");

        hheader << heuristic.get_header(heuristics);
        hsource << heuristic.get_implementation(heuristics);
    }

    Ruleset ruleset = drv.parse_ruleset("../rules/test.rule");

    std::ofstream header("../compiled/test_rules.hpp");
    std::ofstream source("../compiled/test_rules.cpp");

    header << ruleset.get_header(heuristics);

    source << ruleset.get_implementation(heuristics);

    return 0;
}
