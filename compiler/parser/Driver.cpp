#include <memory>
#include "Driver.h"
#include "../generated/Parser.hpp"

Ruleset Driver::parse_ruleset(const std::string &f) {
    mode = 1;
    first = true;

    file = f;

    location.initialize (&file);
    begin_file(file);

    ExpressionPtr exp;
    Ruleset ruleset;
    Heuristic heuristic;
    yy::parser parser {*this, ruleset, heuristic};
    err = parser.parse();

    return ruleset;
}

Heuristic Driver::parse_heuristic(const std::string &f) {
    mode = 2;
    first = true;

    file = f;

    location.initialize (&file);
    begin_file(file);

    ExpressionPtr exp;
    Ruleset ruleset;
    Heuristic heuristic;
    yy::parser parser {*this, ruleset, heuristic};
    err = parser.parse();

    return heuristic;
}