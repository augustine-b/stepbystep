#ifndef DRIVER_H
#define DRIVER_H

#include <string>
#include <map>
#include "../generated/Parser.hpp"
#include "../script/Rule.h"
#include "../script/Ruleset.h"

# define YY_DECL \
  yy::parser::symbol_type yylex (Driver& drv)

YY_DECL;

class Driver
{
public:

    int err;

    int mode;

    bool first;

    bool pattern;

    std::string file;

    yy::location location;

    Driver() : err {}, mode {}, pattern {}, file {}, location {} {};

    Ruleset parse_ruleset(const std::string &file);

    Heuristic parse_heuristic(const std::string &file);

    void begin_string(const std::string& str);

    void begin_file(const std::string& file);

    void set_pattern() { pattern = true; }

    void unset_pattern() { pattern = false; }
};

#endif
