#ifndef SYMBOLNODE_H
#define SYMBOLNODE_H

#include "ExpressionNode.h"

class SymbolNode : public ExpressionNode
{
    const std::string name;

public:

    explicit SymbolNode(std::string name)
            : name(std::move(name)) {};

    std::string to_reference_string(Context&) const override {
        return "*" + name;
    }

    std::string to_pointer_string(Context& ctx) const override {
        ctx.symbolRefs[name]--;

        if (ctx.symbolRefs[name]) {
            return name + "->clone()";
        } else {
            return "std::move(" + name + ")";
        }
    }

    void count_for_pointer(Context &ctx) const override {
        ctx.symbolRefs[name]++;
    }

    std::string to_pattern_string() const override {
        std::cerr << "Symbol node should not appear in pattern" << std::endl;
        return "";
    }

    std::pair<const Type&, const Type&> accepted_types(Context& ctx) const override {
        return { Type::EXPRESSION, Type::EXPRESSION };
    }

    std::string coerce_reference(Context& ctx, const Type& t) const override {
        return to_reference_string(ctx);
    }
};

#endif
