#ifndef COMPILER_CONSTANTNODE_H
#define COMPILER_CONSTANTNODE_H

#include "ExpressionNode.h"
#include "../script/Context.h"

class ConstantNode : public ExpressionNode
{
public:

    double val;

    explicit ConstantNode(double val)
            : val(val) {};

    std::string to_reference_string(Context&) const override {
        if (val < 0) {
            return "OperatorNode {Operator::MINUS, std::make_unique<ConstantNode>(" + std::to_string(val) + ")}";
        } else {
            return "ConstantNode {" + std::to_string(val) + "}";
        }
    }

    std::string to_pointer_string(Context&) const override {
        return "ConstantNode::make(" + std::to_string(val) + ")";
    }

    std::string to_pattern_string() const override {
        return "ConstantNode::make(" + std::to_string(val) + ")";
    }

    std::pair<const Type&, const Type&> accepted_types(Context& ctx) const override {
        if (static_cast<long>(val) == val) {
            return { Type::INTEGER, Type::EXPRESSION };
        }

        return { Type::DECIMAL, Type::EXPRESSION };
    }

    std::string coerce_reference(Context& ctx, const Type& t) const override {
        if (t == Type::INTEGER) {
            return std::to_string(static_cast<long>(val));
        } else if (t == Type::DECIMAL) {
            return std::to_string(val);
        } else if (t == Type::EXPRESSION) {
            if (val < 0) {
                return "OperatorNode {Operator::MINUS, std::make_unique<ConstantNode>(" + std::to_string(val) + ")}";
            } else {
                return "ConstantNode {" + std::to_string(val) + "}";
            }
        }
    }

    bool is_simple() const override {
        return true;
    }

    bool is_zero() const override {
        return val == 0;
    }
};

#endif
