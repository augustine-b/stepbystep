#ifndef COMPILER_FUNCTIONNODE_H
#define COMPILER_FUNCTIONNODE_H

#include <vector>
#include "ExpressionNode.h"
#include "../keywords/Function.h"

class FunctionNode : public ExpressionNode
{
    Function f;

    std::vector<ExpressionPtr> parameters;

public:

    FunctionNode(Function f, std::vector<ExpressionPtr> parameters)
            : f(std::move(f)), parameters(std::move(parameters)) {};

    std::string to_reference_string(Context& ctx) const override {
        return to_pointer_string(ctx);
    }

    std::string to_pointer_string(Context& ctx) const override {
        std::string param_string;

        for (auto& en : parameters) {
            if (!param_string.empty()) {
                param_string += ", ";
            }
            param_string += en->to_reference_string(ctx);
        }

        return "util::" + f.name + "(" + param_string + ")";
    }

    std::pair<const Type&, const Type&> accepted_types(Context& ctx) const override {
        return f.accepted_types;
    }

    bool can_be_reference(Context& ctx) const override {
        return false;
    }

    std::string coerce_reference(Context& ctx, const Type& t) const override {
        std::string param_string;

        for (auto& en : parameters) {
            if (!param_string.empty()) {
                param_string += ", ";
            }
            param_string += en->coerce_reference(ctx, t);
        }

        if (t == Type::LOGICAL) {
            return "util::" + f.name + "_bool(" + param_string + ")";
        } else if (t == Type::INTEGER) {
            return "util::" + f.name + "_int(" + param_string + ")";
        } else if (t == Type::DECIMAL) {
            return "util::" + f.name + "_double(" + param_string + ")";
        }
    }

    std::string coerce_pointer(Context& ctx, const Type& t) const override {
        std::string param_string;

        for (auto& en : parameters) {
            if (!param_string.empty()) {
                param_string += ", ";
            }
            param_string += en->to_reference_string(ctx);
        }

        return "util::" + f.name + "(" + param_string + ")";
    }

    void count_for_reference(Context &ctx) const override {
        for (auto& en : parameters) {
            en->count_for_reference(ctx);
        }
    }

    void count_for_pointer(Context &ctx) const override {
        for (auto& en : parameters) {
            en->count_for_reference(ctx);
        }
    }

    std::string to_pattern_string() const override {
        std::cerr << "Function node should not appear in pattern" << std::endl;
        return "";
    }
};

#endif
