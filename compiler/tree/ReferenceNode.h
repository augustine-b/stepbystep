#ifndef STEPBYSTEP_REFERENCENODE_H
#define STEPBYSTEP_REFERENCENODE_H

#include "ExpressionNode.h"

class ReferenceNode : public ExpressionNode
{
public:

    int id;

    explicit ReferenceNode(int id) : id {id} {};

    std::string to_reference_string(Context& ctx) const override {
        if (ctx.reference_types[id] == Type::EXPRESSION) {
            return "*ref" + std::to_string(id);
        }

        return "ref" + std::to_string(id);
    }

    std::string to_pointer_string(Context&) const override {
        return "ref" + std::to_string(id) + "->clone()";
    }

    std::string to_pattern_string() const override {
        throw "references not allowed in patterns";
    }

    std::pair<const Type&, const Type&> accepted_types(Context& ctx) const override {
        return { ctx.reference_types[id], ctx.reference_types[id] };
    }

    std::string coerce_reference(Context& ctx, const Type& t) const override {
       return to_reference_string(ctx);
    }
};

#endif
