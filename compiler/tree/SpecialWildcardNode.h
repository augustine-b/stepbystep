#ifndef COMPILER_SPECIALWILDCARDNODE_H
#define COMPILER_SPECIALWILDCARDNODE_H

#include "ExpressionNode.h"

class SpecialWildcardNode : public ExpressionNode
{
public:

    int id;

    bool in_heuristic;

    explicit SpecialWildcardNode(int id, bool in_heuristic)
            : id {id}, in_heuristic {in_heuristic} {};

    std::string to_reference_string(Context&) const override {
        if (in_heuristic) {
            return "*svalue" + std::to_string(id);
        }

        return "*(match->special_wildcards[" + std::to_string(id) + "])";
    }

    std::string to_pointer_string(Context& ctx) const override {
        if (in_heuristic) {
            return "svalue" + std::to_string(id);
        }

        ctx.specialWildcardRefs[id]--;

        if (ctx.specialWildcardRefs[id]) {
            return "match->special_wildcards[" + std::to_string(id) + "]->clone()";
        } else {
            return "std::move(match->special_wildcards[" + std::to_string(id) + "])";
        }
    }

    std::pair<const Type&, const Type&> accepted_types(Context& ctx) const override {
        return { Type::EXPRESSION, Type::EXPRESSION };
    }

    std::string coerce_reference(Context& ctx, const Type& t) const override {
        return to_reference_string(ctx);
    }

    void count_for_pointer(Context &ctx) const override {
        ctx.specialWildcardRefs[id]++;
    }

    std::string to_pattern_string() const override {
        return "std::make_unique<SpecialWildcardNode>(" + std::to_string(id) + ")";
    }

    SpecialWildcardNode* is_special_wildcard() override {
        return this;
    }

    const SpecialWildcardNode* is_special_wildcard() const override {
        return this;
    }
};

#endif
