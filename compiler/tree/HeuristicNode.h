#ifndef STEPBYSTEP_HEURISTICNODE_H
#define STEPBYSTEP_HEURISTICNODE_H

#include <vector>
#include "../script/Heuristic.h"
#include "ExpressionNode.h"

class HeuristicNode : public ExpressionNode
{
    std::string name;

    std::vector<ExpressionPtr> parameters;

public:

    HeuristicNode(std::string name, std::vector<ExpressionPtr> parameters)
            : name {std::move(name)}, parameters {std::move(parameters)} {};

    std::string to_reference_string(Context& ctx) const override {
        return to_pointer_string(ctx);
    }

    std::string to_pointer_string(Context& ctx) const override {
        std::string param_string;

        for (auto& en : parameters) {
            if (!param_string.empty()) {
                param_string += ", ";
            }
            param_string += en->to_reference_string(ctx);
        }

        return "heuristic::prove_" + name + "(container, " + param_string + ")";
    }

    std::pair<const Type&, const Type&> accepted_types(Context& ctx) const override {
        const Heuristic& heuristic = ctx.find_heuristic(name);
        return { heuristic.returnType, heuristic.returnType };
    }

    bool can_be_reference(Context& ctx) const override {
        return false;
    }

    std::string coerce_reference(Context& ctx, const Type& t) const override {
        std::string param_string;

        for (auto& en : parameters) {
            if (!param_string.empty()) {
                param_string += ", ";
            }
            param_string += en->to_reference_string(ctx);
        }

        if (t == Type::LOGICAL || t == Type::INTEGER || t == Type::DECIMAL) {
            return "heuristic::prove_" + name + "(container, " + param_string + ")";
        }
    }

    std::string coerce_pointer(Context& ctx, const Type& t) const override {
        std::string param_string;

        for (auto& en : parameters) {
            if (!param_string.empty()) {
                param_string += ", ";
            }
            param_string += en->to_reference_string(ctx);
        }

        return "heuristic::prove_" + name + "(" + param_string + ")";
    }

    void count_for_reference(Context &ctx) const override {
        for (auto& en : parameters) {
            en->count_for_reference(ctx);
        }
    }

    void count_for_pointer(Context &ctx) const override {
        for (auto& en : parameters) {
            en->count_for_reference(ctx);
        }
    }

    std::string to_pattern_string() const override {
        throw "Heuristic node should not appear in pattern";
    }
};


#endif
