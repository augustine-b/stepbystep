#ifndef COMPILER_WILDCARDNODE_H
#define COMPILER_WILDCARDNODE_H

#include "ExpressionNode.h"

class WildcardNode : public ExpressionNode
{
public:

    int id;

    bool in_heuristic;

    explicit WildcardNode(int id, bool in_heuristic)
            : id {id}, in_heuristic {in_heuristic} {};

    std::string to_reference_string(Context&) const override {
        if (in_heuristic) {
            return "value" + std::to_string(id);
        }
        return "*(match->wildcards[" + std::to_string(id) + "])";
    }

    std::string to_pointer_string(Context&) const override {
        if (in_heuristic) {
            return "value" + std::to_string(id) + ".clone()";
        }
        return "match->wildcards[" + std::to_string(id) + "]->clone()";
    }

    std::string to_pattern_string() const override {
        return "std::make_unique<WildcardNode>(" + std::to_string(id) + ")";
    }

    WildcardNode* is_wildcard() override {
        return this;
    }

    const WildcardNode* is_wildcard() const override {
        return this;
    }

    std::pair<const Type&, const Type&> accepted_types(Context& ctx) const override {
        return { Type::EXPRESSION, Type::EXPRESSION };
    }

    std::string coerce_reference(Context& ctx, const Type& t) const override {
        return to_reference_string(ctx);
    }
};

#endif
