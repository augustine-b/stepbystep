#ifndef COMPILER_OPERATORNODE_H
#define COMPILER_OPERATORNODE_H

#include <vector>
#include <iostream>
#include "ExpressionNode.h"
#include "../../common/Operator.h"
#include "../script/Context.h"
#include "ConstantNode.h"

class OperatorNode : public ExpressionNode
{
public:

    Operator op;

    std::vector<ExpressionPtr> operands;

    OperatorNode(Operator op, ExpressionPtr operand)
            : op(std::move(op)), operands() {
        operands.push_back(std::move(operand));
    }

    OperatorNode(Operator op, ExpressionPtr operand1, ExpressionPtr operand2)
            : op(std::move(op)), operands() {
        if (op == Operator::PLUS || op == Operator::MULTIPLY) {
            if (auto on = operand1->is_operation(op)) {
                for (auto& operand : on->operands) {
                    operands.push_back(std::move(operand));
                }
            } else {
                operands.push_back(std::move(operand1));
            }

            if (auto on = operand2->is_operation(op)) {
                for (auto& operand : on->operands) {
                    operands.push_back(std::move(operand));
                }
            } else {
                operands.push_back(std::move(operand2));
            }
        } else {
            operands.push_back(std::move(operand1));
            operands.push_back(std::move(operand2));
        }
    }

    std::string to_pointer_string(Context& ctx) const override {
        return to_string([&](ExpressionRef expr) { return expr.to_pointer_string(ctx); });
    }

    std::string to_pattern_string() const override {
        return to_string([](ExpressionRef expr) { return expr.to_pattern_string(); });
    }

    std::pair<const Type&, const Type&> accepted_types(Context& ctx) const override {
        if (op == Operator::MINUS && dynamic_cast<ConstantNode*>(operands[0].get())) {
            return operands[0]->accepted_types(ctx);
        }

        return { Type::EXPRESSION, Type::EXPRESSION };
    }

    std::string coerce_reference(Context& ctx, const Type& t) const override {
        if (t == Type::EXPRESSION) {
            return to_reference_string(ctx);
        } else {
            return to_string([&](ExpressionRef expr) { return expr.coerce_reference(ctx, t); });
        }
    }

    std::string to_reference_string(Context& ctx) const override {
        std::string str = "OperatorNode {Operator::" + op.name;
        for (auto& operand : operands) {
            str += ", " + operand->to_pointer_string(ctx);
        }
        str += "}";
        return str;
    }

    void count_for_reference(Context &ctx) const override {
        for (auto& operand : operands) {
            operand->count_for_pointer(ctx);
        }
    }

    void count_for_pointer(Context &ctx) const override {
        for (auto& operand : operands) {
            operand->count_for_pointer(ctx);
        }
    }


    bool is_trivial_pattern() const override {
        return op.arity == 2 && !operands[0]->is_operation() && !operands[1]->is_operation();
    }

    bool is_simple_pattern() const override {
        if (op.arity == -1 || op == Operator::EQUALS) {
            return false;
        }

        for (const auto& operand : operands) {
            if (!operand->is_simple_pattern()) {
                return false;
            }
        }

        return true;
    }

    int complex_pattern_count() const override {
        int cnt {};

        if (op.arity == -1 || op == Operator::EQUALS) {
            cnt++;
        }

        for (const auto& operand : operands) {
            cnt += operand->complex_pattern_count();
        }

        return cnt;
    }

    bool is_simple() const override {
        for (const auto& operand : operands) {
            if (!operand->is_simple()) {
                return false;
            }
        }

        return true;
    }

    OperatorNode* is_operation() override {
        return this;
    }

    OperatorNode* is_operation(const Operator& op) override {
        if (op == this->op) {
            return this;
        }

        return nullptr;
    }

    const OperatorNode* is_operation() const override {
        return this;
    }

    const OperatorNode* is_operation(const Operator& op) const override {
        if (op == this->op) {
            return this;
        }

        return nullptr;
    }

private:

    template <typename T>
    std::string to_string(T func) const {
        if (op.arity == 1 && !op.function) {
            bool is_operator = dynamic_cast<OperatorNode*>(operands[0].get());

            if (is_operator) {
                return op.str + "(" + func(*operands[0]) + ")";
            } else {
                return op.str + func(*operands[0]);
            }
        }

        std::string separator;

        if (op.function) {
            separator = ", ";
        } else {
            separator = " " + op.str + " ";
        }

        std::string str;

        for (auto& en : operands) {
            if (!str.empty()) {
                str += separator;
            }

            bool is_operator = dynamic_cast<OperatorNode*>(en.get());

            if (is_operator) {
                str += "(";
                str += func(*en);
                str += ")";
            } else {
                str += func(*en);
            }
        }

        if (op.function) {
            return op.str + "(" + str + ")";
        }

        return str;
    }
};

#endif
