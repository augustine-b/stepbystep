#ifndef COMPILER_EXPRESSIONNODE_H
#define COMPILER_EXPRESSIONNODE_H

#include <string>
#include <memory>
#include <iostream>
#include "../../common/Operator.h"
#include "../keywords/Type.h"

class OperatorNode;
class WildcardNode;
class SpecialWildcardNode;
class Context;

class ExpressionNode
{
public:

    virtual ~ExpressionNode() = default;

    // KILL
    virtual std::string to_reference_string(Context& ctx) const = 0;

    virtual std::string to_pointer_string(Context& ctx) const = 0;
    // THIS TWO ^

    virtual std::string to_pattern_string() const = 0;

    virtual std::pair<const Type&, const Type&> accepted_types(Context& ctx) const = 0;

    virtual bool can_be_reference(Context& ctx) const { return true; }

    virtual std::string coerce_reference(Context& ctx, const Type& t) const = 0;

    virtual std::string coerce_pointer(Context& ctx, const Type& t) const {}

    virtual void count_for_reference(Context&) const {}

    virtual void count_for_pointer(Context&) const {}

    virtual bool is_trivial_pattern() const { return false; }

    virtual bool is_simple_pattern() const { return true; }

    virtual int complex_pattern_count() const { return 0; }

    virtual bool is_simple() const { return false; }

    virtual bool is_zero() const { return false; }

    virtual OperatorNode* is_operation() { return nullptr; }

    virtual OperatorNode* is_operation(const Operator&) { return nullptr; }

    virtual WildcardNode* is_wildcard() { return nullptr; }

    virtual SpecialWildcardNode* is_special_wildcard() { return nullptr; }

    virtual const OperatorNode* is_operation() const { return nullptr; }

    virtual const OperatorNode* is_operation(const Operator&) const { return nullptr; }

    virtual const WildcardNode* is_wildcard() const { return nullptr; }

    virtual const SpecialWildcardNode* is_special_wildcard() const { return nullptr; }
};

using ExpressionPtr = std::unique_ptr<ExpressionNode>;

using ExpressionRef = const ExpressionNode&;

#endif
