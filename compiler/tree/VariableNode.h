#include <utility>

#ifndef COMPILER_VARIABLENODE_H
#define COMPILER_VARIABLENODE_H

#include "ExpressionNode.h"

class VariableNode : public ExpressionNode
{
    std::string name;

public:

    explicit VariableNode(std::string name)
            : name(std::move(name)) {};

    std::string to_reference_string(Context&) const override {
        return "VariableNode {\"" + name + "\"}";
    }

    std::string to_pointer_string(Context&) const override {
        return "std::make_unique<VariableNode>(\"" + name + "\")";
    }

    std::string to_pattern_string() const override {
        return "std::make_unique<VariableNode>(\"" + name + "\")";
    }

    bool is_simple() const override {
        return true;
    }

    std::pair<const Type&, const Type&> accepted_types(Context& ctx) const override {
        return { Type::EXPRESSION, Type::EXPRESSION };
    }

    std::string coerce_reference(Context& ctx, const Type& t) const override {
        return to_reference_string(ctx);
    }
};

#endif
