#include <ostream>
#include "Ruleset.h"
#include "Heuristic.h"

std::ostream& operator<<(std::ostream& out, const Ruleset& ruleset) {
    return out << "ruleset " << ruleset.name;
}

std::ostream& operator<<(std::ostream& out, const Rule& rule) {
    return out << "rule " << rule.name;
}

std::ostream& operator<<(std::ostream& out, const Heuristic& heuristic) {
    return out << "rule " << heuristic.name;
}

std::ostream& operator<<(std::ostream &out, const HeuristicCase&) {
    return out << "heuristic";
}