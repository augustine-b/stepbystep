#include <utility>

#ifndef STEPBYSTEP_REFERENCECOUNTER_H
#define STEPBYSTEP_REFERENCECOUNTER_H

#include <string>
#include <vector>
#include <map>
#include "CodeBuilder.h"
#include "../tree/ExpressionNode.h"

class Heuristic;

class Context
{
    CodeBuilder builder;

public:

    std::map<std::string, int> symbolRefs;

    std::array<int, 10> specialWildcardRefs;

    const std::vector<Heuristic>& heuristics;

    std::vector<Type> reference_types;

    explicit Context(const std::vector<Heuristic>& heuristics, std::string_view header)
        : builder {header}, symbolRefs {}, specialWildcardRefs {}, heuristics {heuristics} {}

    Context(const Context&) = delete;

    void reset_refs() {
        symbolRefs.clear();
        for (auto& ref : specialWildcardRefs) {
            ref = 0;
        }
    }

    template <typename T>
    Context& operator<<(T output) {
        builder << output;
        return *this;
    }

    std::string str() {
        return builder.str();
    }

    const Heuristic& find_heuristic(std::string_view name);
};

#endif
