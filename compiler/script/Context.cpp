#include "Heuristic.h"

const Heuristic& Context::find_heuristic(std::string_view name) {
    for (const auto& heuristic : heuristics) {
        if (heuristic.name == name) {
            return heuristic;
        }
    }

    std::cerr << "Could not find heuristic named " << name << std::endl;
}