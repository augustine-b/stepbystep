#ifndef STEPBYSTEP_HEURISTICCASE_H
#define STEPBYSTEP_HEURISTICCASE_H

#include <string>
#include "HeuristicPatternCompiler.h"
#include "instructions/Instruction.h"

class HeuristicCase
{
    std::vector<ExpressionPtr> patterns;

    std::vector<InstructionPtr> instructions;

public:

    HeuristicCase() : patterns {}, instructions {} {}

    explicit HeuristicCase(std::vector<ExpressionPtr> patterns)
        : patterns {std::move(patterns)}, instructions {} {}

    friend std::ostream& operator<<(std::ostream& out, const HeuristicCase& ruleset);

    void get_definition(Context& ctx) const {
        ctx << "do" << Elem::OPEN;

        compile_pattern(ctx, patterns);

        for (auto& instruction : instructions) {
            instruction->compile(ctx);
        }

        ctx << Elem::CLOSE << "while(false)" << Elem::END;
    }

    void add_instruction(InstructionPtr instruction) {
        instructions.push_back(std::move(instruction));
    }
};

#endif
