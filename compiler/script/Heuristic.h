#include <utility>

#include <utility>

#ifndef STEPBYSTEP_HEURISTIC_H
#define STEPBYSTEP_HEURISTIC_H

#include <string>
#include <vector>
#include "Context.h"
#include "HeuristicCase.h"
#include "../keywords/Type.h"
#include "CompilationUtils.h"

class Heuristic
{
public:

    std::string name;

    std::vector<Type> parameterTypes;

    Type returnType;

    std::vector<HeuristicCase> cases;

    Heuristic() : name {}, parameterTypes {}, returnType {Type::NO_TYPE}, cases {} {}

    explicit Heuristic(std::string name, std::vector<Type> parameterTypes, Type returnType)
        : name(std::move(name)), parameterTypes {std::move(parameterTypes)},
        returnType {std::move(returnType)}, cases() {}

    friend std::ostream& operator<<(std::ostream& out, const Heuristic& heuristic);

    void add_case(HeuristicCase&& heuristicCase) {
        cases.push_back(std::move(heuristicCase));
    }

    std::string get_header(const std::vector<Heuristic>& heuristics) const {
        std::string includes;
        includes += "#ifndef HEURISTIC_" + name + "_H\n";
        includes += "#define HEURISTIC_" + name + "_H\n\n";
        includes += header_includes;

        Context ctx {heuristics, includes};

        ctx << Elem::NAMESPACE << "heuristic";

        std::string refs = comma_sep(parameterTypes, [](auto x) { return x.refname; });
        std::string ptrs = comma_sep(parameterTypes, [](auto x) { return x.ptrname; });

        ctx << (returnType.ptrname + " prove_" + name + "(AssumptionContainer&, " + refs + ")") << Elem::END << Elem::SKIP;

        ctx << ("void assume_" + name + "(AssumptionContainer&, " + ptrs + ", " + returnType.refname + ")") << Elem::END << Elem::SKIP;

        build_assumed(ctx);

        ctx << Elem::CLOSE;

        ctx << "#endif";

        return ctx.str();
    }

    std::string get_implementation(const std::vector<Heuristic>& heuristics) const {
        std::string includes {implementation_includes};

        for (const Heuristic& heuristic : heuristics) {
            includes += "#include \"" + heuristic.name + "_heuristic.hpp\"\n";
        }
        includes += '\n';

        Context ctx {heuristics, includes};

        ctx << Elem::NAMESPACE << "heuristic";

        build_assume(ctx);

        build_prove(ctx);

        ctx << Elem::CLOSE;

        return ctx.str();
    }

private:

    void build_assume(Context& ctx) const {
        std::string input = comma_sep(parameterTypes, [](auto x, auto y) { return x.ptrname + " " + y; });

        std::string emplace_list = comma_sep(parameterTypes, [](auto, auto y) { return "std::move(" + y + ")"; });

        ctx << ("void assume_" + name + "(AssumptionContainer& container, " + input + ", " + returnType.ptrname + " value)") << Elem::OPEN;

        ctx << ("container." + name + "_assumptions.emplace_back(" + emplace_list + ", value)") << Elem::END;

        ctx << Elem::CLOSE;
    }

    void build_assumed(Context& ctx) const {
        ctx << ("class assumed_" + name) << Elem::SKIP << Elem::OPENB;

        ctx << "size_t index = 0" << Elem::END << Elem::SKIP;
        ctx << "AssumptionContainer& container" << Elem::END << Elem::SKIP;

        for (size_t i = 0; i < parameterTypes.size(); i++) {
            ctx << (parameterTypes[i].optname + " " + static_cast<char>('a' + i)) << Elem::END;
        }
        ctx << (returnType.optname + " value") << Elem::END;

        ctx << Elem::SKIP << "public:" << Elem::SKIP << Elem::SKIP;

        std::string constructor {"assumed_" + name + "(AssumptionContainer& container, "};
        constructor += comma_sep(parameterTypes, [](auto x, auto y) { return x.optname + " " + y; });
        constructor += ", " + returnType.optname + " value) : container {container}, ";
        constructor += comma_sep(parameterTypes, [](auto, auto y) { return y + " {std::move(" + y + ")}"; });
        constructor += ", value {std::move(value)} {}";

        ctx << constructor << Elem::SKIP << Elem::SKIP;

        std::string next_assumption {"std::tuple<"};
        next_assumption += comma_sep(parameterTypes, [](auto x) { return x.ptrname; });
        next_assumption += ", " + returnType.ptrname + ">* next_assumption()";

        ctx << next_assumption << Elem::OPEN;

        ctx << Elem::WHILE << ("index < container." + name + "_assumptions.size()");

        std::string current_elem {"auto& ["};
        current_elem += comma_sep(parameterTypes, [](auto, auto y){ return y + "1"; });
        current_elem += ", value1] = container." + name + "_assumptions[index]";

        ctx << current_elem << Elem::END;

        std::string condition;

        for (size_t i = 0; i < parameterTypes.size(); i++) {
            char curr = static_cast<char>('a' + i);
            condition += std::string{"(!"} + curr + " || *" + curr + " == ";
            if (parameterTypes[i].needs_deref) {
                condition += "*";
            }
            condition += curr + std::string{"1) &&"};
        }
        condition += "(!value || *value == ";
        if (returnType.needs_deref) {
            condition += "*";
        }
        condition += "value1)";

        ctx << Elem::IF << condition;

        ctx << Elem::RETURN << ("&container." + name + "_assumptions[index++]");

        ctx << Elem::ELSE;

        ctx << "index++" << Elem::END;

        ctx << Elem::CLOSE;

        ctx << Elem::CLOSE;

        ctx << Elem::RETURN << "{}";
        ctx << Elem::CLOSE;

        ctx << Elem::CLOSE << Elem::END;
    }

    void build_prove(Context& ctx) const {
        std::string input = comma_sep(parameterTypes, [](auto x, auto y) { return x.refname + " " + y; });

        ctx << (returnType.ptrname + " prove_" + name + "(AssumptionContainer& container, " + input + ")") << Elem::OPEN;

        for (auto& heuristicCase : cases) {
            heuristicCase.get_definition(ctx);
        }

        ctx << Elem::CLOSE;
    }

    static constexpr std::string_view header_includes {
            "#include \"../runtime/interpreter/AssumptionContainer.h\"\n"
            "#include \"../runtime/tree/ExpressionNode.h\"\n\n"
    };

    static constexpr std::string_view implementation_includes {
            "#include \"../runtime/interpreter/GeneralMatcher.h\"\n"
            "#include \"../runtime/interpreter/AssumptionContainer.h\"\n"
            "#include \"../runtime/tree/WildcardNode.h\"\n"
            "#include \"../runtime/tree/VariableNode.h\"\n"
            "#include \"../runtime/tree/ConstantNode.h\"\n"
            "#include \"../runtime/utility/Functions.h\"\n"
            "#include \"../runtime/utility/Operators.h\"\n"
            "#include \"../runtime/Debug.h\"\n"
    };
};

#endif
