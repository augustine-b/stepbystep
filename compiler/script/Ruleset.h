#ifndef RULESET_H
#define RULESET_H

#include <string>
#include <string_view>
#include <algorithm>
#include "CodeBuilder.h"
#include "Rule.h"

class Ruleset
{
public:

    Ruleset() : name(), rules() {}

    explicit Ruleset(std::string name)
            : name(std::move(name)), rules() {}

    friend std::ostream& operator<<(std::ostream& out, const Ruleset& ruleset);

    void add_rule(Rule&& rule) {
        rules.push_back(std::move(rule));
    }

    std::string get_header(const std::vector<Heuristic>& heuristics) const {
        Context ctx {heuristics, header_includes};

        ctx << Elem::NAMESPACE << ("rulesets::" + name);

        for (auto& rule : rules) {
            rule.get_declaration(ctx);
        }
        ctx << Elem::SKIP;

        ctx << Elem::RULE_ARRAY << "rules";
        for (auto& rule : rules) {
            ctx << rule.name;
        }
        ctx << Elem::CLOSE << Elem::SKIP;

        build_persistents(ctx);

        build_collapses(ctx);

        build_transparents(ctx);

        build_skipifs(ctx);

        ctx << Elem::CLOSE;

        return ctx.str();
    }

    std::string get_implementation(const std::vector<Heuristic>& heuristics) const {
        Context ctx {heuristics, implementation_includes};

        ctx << Elem::NAMESPACE << ("rulesets::" + name);

        for (auto& rule : rules) {
            rule.get_definition(ctx);
        }

        ctx << Elem::CLOSE;

        return ctx.str();
    }

private:

    static constexpr std::string_view header_includes {
            "#include \"../runtime/interpreter/Change.h\"\n"
            "#include \"../runtime/tree/ExpressionNode.h\"\n\n"
    };

    static constexpr std::string_view implementation_includes {
            "#include \"../runtime/interpreter/Change.h\"\n"
            "#include \"../runtime/interpreter/TrivialMatcher.h\"\n"
            "#include \"../runtime/interpreter/SimpleMatcher.h\"\n"
            "#include \"../runtime/interpreter/ComplexMatcher.h\"\n"
            "#include \"../runtime/interpreter/GeneralMatcher.h\"\n"
            "#include \"../runtime/tree/WildcardNode.h\"\n"
            "#include \"../runtime/tree/SpecialWildcardNode.h\"\n"
            "#include \"../runtime/tree/VariableNode.h\"\n"
            "#include \"../runtime/tree/ConstantNode.h\"\n"
            "#include \"../runtime/utility/Functions.h\"\n"
            "#include \"../runtime/utility/Operators.h\"\n"
            "#include \"../runtime/Debug.h\"\n\n"
    };

    std::string name;

    std::vector<Rule> rules;

    void build_persistents(Context& ctx) const {
        std::string tmp;

        for (auto& rule : rules) {
            tmp += (rule.persistent ? "1" : "0");
        }
        ctx << Elem::BITSET << "persistents" << ("\"" + tmp + "\"") << Elem::SKIP;
    }

    void build_collapses(Context& ctx) const {
        std::string tmp;

        for (auto& rule : rules) {
            tmp += (rule.collapse ? "1" : "0");
        }
        ctx << Elem::BITSET << "collapses" << ("\"" + tmp + "\"") << Elem::SKIP;
    }

    void build_transparents(Context& ctx) const {
        std::string tmp;

        for (auto& rule : rules) {
            tmp += (rule.transparent ? "1" : "0");
        }
        ctx << Elem::BITSET << "transparents" << ("\"" + tmp + "\"") << Elem::SKIP;
    }

    void build_skipifs(Context& ctx) const {
        ctx << Elem::BITSET_ARRAY << rules.size() << "skipifs";
        for (auto& rule : rules) {
            if (!rule.skipifs.empty()) {
                std::string tmp(rules.size(), '0');
                for (size_t i = 0; i < rule.skipifs.size(); i++) {
                    auto it = std::find_if(rules.begin(), rules.end(), [&](const auto& x) { return x.name == rule.name;});
                    tmp[it - rules.begin()] = '1';
                }
                ctx << ("immutable_bitset {\"" + tmp + "\"}");
            } else {
                ctx << "{}";
            }
        }
        ctx << Elem::CLOSE;
    }
};

#endif
