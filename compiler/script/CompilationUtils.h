#ifndef STEPBYSTEP_COMPILATIONUTILS_H
#define STEPBYSTEP_COMPILATIONUTILS_H

#include <type_traits>
#include <string>
#include <vector>
#include "../tree/ExpressionNode.h"
#include "Context.h"

template <typename T, typename Func>
typename std::enable_if<std::is_invocable_r_v<std::string, Func, T>, std::string>::type
comma_sep(std::vector<T> vec, Func func) {
    std::string tmp;

    for (size_t i = 0; i < vec.size(); i++) {
        if (i != 0) {
            tmp += ", ";
        }
        tmp += func(vec[i]);
    }

    return tmp;
}

template <typename T, typename Func>
typename std::enable_if<std::is_invocable_r_v<std::string, Func, T, std::string>, std::string>::type
comma_sep(std::vector<T> vec, Func func) {
    std::string tmp;

    for (size_t i = 0; i < vec.size(); i++) {
        if (i != 0) {
            tmp += ", ";
        }
        std::string index_letter {static_cast<char>('a' + i)};

        tmp += func(vec[i], index_letter);
    }

    return tmp;
}

inline void coerce_reference(Context& ctx, ExpressionRef a, ExpressionRef b, std::string_view a_name, std::string_view b_name) {
    const auto& [a_from, a_to] = a.accepted_types(ctx);
    const auto& [b_from, b_to] = b.accepted_types(ctx);

    for (const Type& t : Type::types) {
        if (a_from <= t && t <= a_to && b_from <= t && t <= b_to) {
            if (t == Type::EXPRESSION && !a.can_be_reference(ctx)) {
                ctx << Elem::EXPRESSION_PTR << (std::string {a_name} + "tmp") << a.coerce_pointer(ctx, t);
                ctx << Elem::EXPRESSION_REF << a_name << (std::string {a_name} + "tmp");
            } else {
                ctx << t << a_name << a.coerce_reference(ctx, t);
            }

            if (t == Type::EXPRESSION && !b.can_be_reference(ctx)) {
                ctx << Elem::EXPRESSION_PTR << (std::string {b_name} + "tmp") << b.coerce_pointer(ctx, t);
                ctx << Elem::EXPRESSION_REF << b_name << (std::string {b_name} + "tmp");
            } else {
                ctx << t << b_name << b.coerce_reference(ctx, t);
            }

            return;
        }
    }
}

#endif
