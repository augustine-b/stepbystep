#ifndef STEPBYSTEP_PROVEEQUALITYINSTRUCTION_H
#define STEPBYSTEP_PROVEEQUALITYINSTRUCTION_H

#include "LogicalInstruction.h"
#include "../../tree/ExpressionNode.h"

class ProveEqualityInstruction : public LogicalInstruction
{
    ExpressionPtr lhs;

    ExpressionPtr rhs;

    bool value;

public:

    ProveEqualityInstruction(ExpressionPtr lhs, ExpressionPtr rhs, bool value)
            : lhs(std::move(lhs)), rhs(std::move(rhs)), value(value) {};

    void build(Context& ctx, bool flip, const std::function<void()> &fn) const override {
        ctx << Elem::OPENB;

        ctx << Elem::EXPRESSION_REF << "lhs" << lhs->to_reference_string(ctx);
        ctx << Elem::EXPRESSION_REF << "rhs" << rhs->to_reference_string(ctx);

        ctx << Elem::INT << "actual" << "prove_equals(lhs, rhs)";

        ctx << Elem::DEBUG << ("\"proving: equality between \"") << "lhs.to_string()"
            << "\" and \"" << "rhs.to_string()" << "\" : \"" << "actual" << Elem::END;

        ctx << Elem::IF;

        std::string tmp;
        tmp += "actual";
        tmp += flip ? " != " : " == ";
        tmp += (value ? "-1" : "1");

        ctx << tmp;

        fn();

        ctx << Elem::CLOSE << Elem::CLOSE;
    }

    bool compile_propagated(Context& ctx) override {
        if (lhs->is_wildcard() && rhs->is_simple()) {
            build_constraint(ctx, *lhs, *rhs);
            return true;
        }

        if (rhs->is_wildcard() && lhs->is_simple()) {
            build_constraint(ctx, *rhs, *lhs);
            return true;
        }

        return false;
    }

    void count_references(Context& ctx) const override {
        lhs->count_for_reference(ctx);
        rhs->count_for_reference(ctx);
    }

private:

    void build_constraint(Context& ctx, ExpressionRef wildcard, ExpressionRef expression) {
        ctx << Elem::CONSTRAINT << static_cast<const WildcardNode&>(wildcard).id;

        ctx << Elem::EXPRESSION_REF << "expression" << expression.to_reference_string(ctx);

        std::string relation = value ? "==" : "!=";

        ctx << Elem::INT << "actual" << "prove_equals(expression, built)";

        ctx << Elem::DEBUG << ("\"proving: equality between \"") << "built.to_string()"
            << ("\" and \"") << "expression.to_string()" << "\" : \"" << "actual" << Elem::END;

        ctx << Elem::RETURN << (value ? "actual == 1" : "actual == -1");

        ctx << Elem::CLOSE_CONSTRAINT;
    }
};

#endif
