#ifndef EQUALITYINSTRUCTION_H
#define EQUALITYINSTRUCTION_H

#include "LogicalInstruction.h"
#include "../../tree/ExpressionNode.h"

class EqualityInstruction : public LogicalInstruction
{
    ExpressionPtr lhs;

    ExpressionPtr rhs;

    bool value;

public:

    EqualityInstruction(ExpressionPtr lhs, ExpressionPtr rhs, bool value)
            : lhs(std::move(lhs)), rhs(std::move(rhs)), value(value) {};

    bool compile_propagated(Context& ctx) override {
        if (lhs->is_wildcard() && rhs->is_simple()) {
            build_constraint(ctx, *lhs, *rhs);
            return true;
        }

        if (rhs->is_wildcard() && lhs->is_simple()) {
            build_constraint(ctx, *rhs, *lhs);
            return true;
        }

        return false;
    }

    void count_references(Context& ctx) const override {
        lhs->count_for_reference(ctx);
        rhs->count_for_reference(ctx);
    }

protected:

    void build(Context& ctx, bool flip, const std::function<void()>& fn) const override {
        ctx << Elem::OPENB;

        coerce_reference(ctx, *lhs, *rhs, "lhs", "rhs");

        std::string relation = value ^ flip ? "==" : "!=";

        ctx << Elem::BOOL << "actual" << (std::string("lhs ") + relation + " rhs");

        ctx << Elem::DEBUG << ("\"checking: \"") << "util::to_string(lhs)"
            << ("\"" + relation + "\"") << "util::to_string(rhs)" << "\" : \"" << "actual" << Elem::END;

        ctx << Elem::IF << "actual";

        fn();

        ctx << Elem::CLOSE << Elem::CLOSE;
    }

private:

    void build_constraint(Context& ctx, ExpressionRef wildcard, ExpressionRef expression) {
        ctx << Elem::CONSTRAINT << static_cast<const WildcardNode&>(wildcard).id;

        ctx << Elem::EXPRESSION_REF << "expression" << expression.to_reference_string(ctx);

        std::string relation = value ? "==" : "!=";

        ctx << Elem::BOOL << "actual" << (std::string("built ") + relation + " expression");

        ctx << Elem::DEBUG << ("\"checking: \"") << "built.to_string()"
            << ("\"" + relation + "\"") << "expression.to_string()" << "\" : \"" << "actual" << Elem::END;

        ctx << Elem::RETURN << "actual";

        ctx << Elem::CLOSE_CONSTRAINT;
    }
};

#endif
