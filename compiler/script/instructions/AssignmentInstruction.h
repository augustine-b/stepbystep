#ifndef ASSIGNMENTINSTRUCTION_H
#define ASSIGNMENTINSTRUCTION_H

#include <iostream>
#include "../Ruleset.h"
#include "Instruction.h"
#include "../../tree/ExpressionNode.h"

class AssignmentInstruction : public Instruction
{
    std::vector<std::string> lhs;

    ExpressionPtr rhs;

public:

    AssignmentInstruction(std::vector<std::string> lhs, ExpressionPtr rhs)
            : lhs(std::move(lhs)), rhs(std::move(rhs)) {};

    void compile(Context& ctx) const override {
        if (lhs.size() == 1) {
            ctx << Elem::EXPRESSION_PTR << lhs[0] << rhs->to_pointer_string(ctx);
        } else {
            std::string tmp;
            for (size_t i = 0; i < lhs.size(); i++) {
                if (i != 0) {
                    tmp += ", ";
                }
                tmp += lhs[i];
            }

            ctx << Elem::STRUCTURED_BINDING << tmp << rhs->to_pointer_string(ctx);
        }

        for (const auto& symbol : lhs) {
            ctx << Elem::DEBUG << "\"assigned \"" << (symbol + "->to_string()")
                << ("\" to " + symbol + "\"") << Elem::END;
        }

        ctx << Elem::SKIP;
    }

    void count_references(Context& ctx) const override {
        rhs->count_for_pointer(ctx);
    }
};

#endif
