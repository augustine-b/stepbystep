#ifndef STEPBYSTEP_LOGICALANDINSTRUCTION_H
#define STEPBYSTEP_LOGICALANDINSTRUCTION_H

#include "LogicalInstruction.h"

class LogicalAndInstruction : public LogicalInstruction
{
    std::unique_ptr<LogicalInstruction> left;

    std::unique_ptr<LogicalInstruction> right;

public:

    LogicalAndInstruction(std::unique_ptr<LogicalInstruction> left, std::unique_ptr<LogicalInstruction> right)
            : left(std::move(left)), right(std::move(right)) {}

protected:

    void build(Context& ctx, bool flip, const std::function<void()>& fn) const override {
        if (flip) {
            LogicalInstruction::build_or(ctx, true, *left, *right, fn);
        } else {
            LogicalInstruction::build_and(ctx, false, *left, *right, fn);
        }
    };

    void count_references(Context& ctx) const override {
        left->count_references(ctx);
        right->count_references(ctx);
    }
};

#endif
