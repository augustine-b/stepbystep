#ifndef STEPBYSTEP_BOOLEANINSTRUCTION_H
#define STEPBYSTEP_BOOLEANINSTRUCTION_H

#include <functional>
#include "Instruction.h"

class LogicalInstruction : public Instruction
{
public:

    void compile(Context& ctx) const override {
        build(ctx, true, [&]() { ctx << Elem::CONTINUE; });
    }

    void compile_conditional(Context& ctx, const Instruction& instruction) const {
        build(ctx, false, [&]() { instruction.compile(ctx); });
    }

protected:

    virtual void build(Context& ctx, bool flip, const std::function<void()>& fn) const = 0;

    static void build_and(Context& ctx, bool flip, const LogicalInstruction& left, const LogicalInstruction& right,
            const std::function<void()>& fn) {
        auto lambda = [&]() {
            right.build(ctx, flip, fn);
        };

        left.build(ctx, flip, lambda);
    }

    static void build_or(Context& ctx, bool flip, const LogicalInstruction& left, const LogicalInstruction& right,
              const std::function<void()>& fn) {
        ctx << Elem::OPENB;

        std::string inner = "inner" + std::to_string(reinterpret_cast<long>(&left));
        std::string exit = "exit" + std::to_string(reinterpret_cast<long>(&left));

        auto lambda = [&]() {
            ctx << Elem::GOTO << inner;
        };

        left.build(ctx, flip, lambda);

        right.build(ctx, flip, lambda);

        ctx << Elem::GOTO << exit;

        ctx << (inner + ":") << Elem::SKIP;

        fn();

        ctx << (exit + ":") << Elem::END;

        ctx << Elem::CLOSE;
    }
};

#endif
