#ifndef STEPBYSTEP_CONDITIONALINSTRUCTION_H
#define STEPBYSTEP_CONDITIONALINSTRUCTION_H

#include "LogicalInstruction.h"

class ConditionalInstruction : public Instruction
{
    std::unique_ptr<LogicalInstruction> condition;

    InstructionPtr body;

public:

    ConditionalInstruction(std::unique_ptr<LogicalInstruction> condition, InstructionPtr body)
            : condition {std::move(condition)}, body {std::move(body)} {}

    void compile(Context& ctx) const override {
        condition->compile_conditional(ctx, *body);
    }

    void count_references(Context& ctx) const override {
        condition->count_references(ctx);
        body->count_references(ctx);
    }
};

#endif
