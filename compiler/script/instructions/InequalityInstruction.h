#ifndef STEPBYSTEP_INEQUALITYINSTRUCTION_H
#define STEPBYSTEP_INEQUALITYINSTRUCTION_H

#include <string>
#include "LogicalInstruction.h"
#include "../../tree/ExpressionNode.h"

class InequalityInstruction : public LogicalInstruction
{
    bool less;

    bool strict;

    ExpressionPtr lhs;

    ExpressionPtr rhs;

public:

    InequalityInstruction(std::pair<bool, bool> cmp, ExpressionPtr lhs, ExpressionPtr rhs)
            : less {cmp.first}, strict {cmp.second}, lhs {std::move(lhs)}, rhs {std::move(rhs)} {}

    bool compile_propagated(Context& ctx) override {
        if (lhs->is_wildcard() && rhs->is_simple()) {
            build_constraint(ctx, *lhs, *rhs, false);
            return true;
        }

        if (rhs->is_wildcard() && lhs->is_simple()) {
            build_constraint(ctx, *rhs, *lhs, true);
            return true;
        }

        return false;
    }

    void count_references(Context& ctx) const override {
        lhs->count_for_reference(ctx);
        rhs->count_for_reference(ctx);
    }

protected:

    void build(Context& ctx, bool flip, const std::function<void()>& fn) const override {
        ctx << Elem::OPENB;

        ctx << Elem::EXPRESSION_REF << "lhs" << lhs->to_reference_string(ctx);
        ctx << Elem::EXPRESSION_REF << "rhs" << rhs->to_reference_string(ctx);

        std::string relation = std::string(less ^ flip ? "<" : ">") + (strict ^ flip ? "" : "=");

        ctx << Elem::BOOL << "actual" << (std::string("lhs ") + relation + " rhs");

        ctx << Elem::DEBUG << ("\"checking: \"") << "lhs.to_string()"
                << ("\"" + relation + "\"") << "rhs.to_string()" << "\" : \"" << "actual" << Elem::END;

        ctx << Elem::IF << "actual";

        fn();

        ctx << Elem::CLOSE << Elem::CLOSE;
    }

private:

    void build_constraint(Context& ctx, ExpressionRef wildcard, ExpressionRef expression, bool flip) {
        ctx << Elem::CONSTRAINT << static_cast<const WildcardNode&>(wildcard).id;

        ctx << Elem::EXPRESSION_REF << "expression" << expression.to_reference_string(ctx);

        std::string relation = std::string(less ^ flip ? "<" : ">") + (strict ? "" : "=");

        ctx << Elem::BOOL << "actual" << (std::string("built ") + relation + " expression");

        ctx << Elem::DEBUG << ("\"checking: \"") << "built.to_string()"
            << ("\"" + relation + "\"") << "expression.to_string()" << "\" : \"" << "actual" << Elem::END;

        ctx << Elem::RETURN << "actual";

        ctx << Elem::CLOSE_CONSTRAINT;
    }
};

#endif
