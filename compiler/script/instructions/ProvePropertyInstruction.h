#ifndef STEPBYSTEP_PROVEPROPERTYINSTRUCTION_H
#define STEPBYSTEP_PROVEPROPERTYINSTRUCTION_H

#include "LogicalInstruction.h"
#include "../../keywords/Property.h"
#include "../../tree/ExpressionNode.h"

class ProvePropertyInstruction : public LogicalInstruction
{
    ExpressionPtr lhs;

    Property rhs;

    bool value;

public:

    ProvePropertyInstruction(ExpressionPtr lhs, Property rhs, bool value)
            : lhs(std::move(lhs)), rhs(std::move(rhs)), value(value) {};

    bool compile_propagated(Context& ctx) override {
        if (lhs->is_wildcard()) {
            ctx << Elem::CONSTRAINT << static_cast<WildcardNode&>(*lhs).id;

            eval_property(ctx);

            ctx << Elem::RETURN << (value ? "actual == 1" : "actual == -1");

            ctx << Elem::CLOSE_CONSTRAINT;
            return true;
        }

        return false;
    }

    void count_references(Context& ctx) const override {
        lhs->count_for_reference(ctx);
    }

protected:

    void build(Context& ctx, bool flip, const std::function<void()>& fn) const override {
        ctx << Elem::OPENB;
        ctx << Elem::EXPRESSION_REF << "built" << lhs->to_reference_string(ctx);

        eval_property(ctx);

        ctx << Elem::IF;

        std::string tmp = "actual";
        tmp += flip ? " != " : " == ";
        tmp += value ? "1" : "-1";

        ctx << tmp;

        fn();

        ctx << Elem::CLOSE << Elem::CLOSE;
    }


private:

    void eval_property(Context& ctx) const {
        ctx << Elem::INT << "actual";

        if (rhs == Property::INTEGER) {
            ctx << "prove_integer(built)";
        } else {
            std::cerr << "No proof exists for property: " << rhs.name << std::endl;
            return;
        }

        ctx << Elem::DEBUG << ("\"proving: " + rhs.name + " property of \"")
            << "built.to_string()" << "\" : \"" << "actual" << Elem::END;
    }
};

#endif
