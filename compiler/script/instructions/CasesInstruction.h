#ifndef STEPBYSTEP_CASESINSTRUCTION_H
#define STEPBYSTEP_CASESINSTRUCTION_H

#include <vector>
#include "Instruction.h"
#include "../../tree/ExpressionNode.h"

struct CaseList
{
    std::vector<ExpressionPtr> labels;

    std::vector<InstructionPtr> instructions;

    CaseList() : labels {}, instructions {} {}

    CaseList(ExpressionPtr label, InstructionPtr instruction)
            : labels {}, instructions {} {
        labels.push_back(std::move(label));
        instructions.push_back(std::move(instruction));
    }

    void add_case(ExpressionPtr label, InstructionPtr instruction) {
        labels.push_back(std::move(label));
        instructions.push_back(std::move(instruction));
    }
};

class CasesInstruction : public Instruction
{
    ExpressionPtr expression;

    std::vector<ExpressionPtr> labels;

    std::vector<InstructionPtr> instructions;

    InstructionPtr defaultCase;

public:

    CasesInstruction (ExpressionPtr expression, CaseList caseList, InstructionPtr defaultCase)
            : expression {std::move(expression)}, labels {std::move(caseList.labels)},
              instructions {std::move(caseList.instructions)}, defaultCase {std::move(defaultCase)} {}

    void compile(Context& ctx) const override {
        ctx << Elem::OPENB;

        std::string exit = "exit" + std::to_string(reinterpret_cast<long>(this));

        ctx << Elem::EXPRESSION_REF << "built" << expression->to_reference_string(ctx);

        for (size_t i = 0; i < labels.size(); i++) {
            ctx << Elem::OPENB;

            ctx << Elem::EXPRESSION_REF << "temp" << labels[i]->to_reference_string(ctx);

            ctx << Elem::IF << "built == temp";

            instructions[i]->compile(ctx);

            ctx << Elem::GOTO << exit;

            ctx << Elem::CLOSE << Elem::CLOSE;
        }

        if (defaultCase) {
            ctx << Elem::OPENB;
            defaultCase->compile(ctx);
            ctx << Elem::CLOSE;
        }

        ctx << (exit + ":") << Elem::END;

        ctx << Elem::CLOSE;
    }

    void count_references(Context& ctx) const override {
        expression->count_for_reference(ctx);
        for (auto& label : labels) {
            label->count_for_reference(ctx);
        }
        for (auto& instr : instructions) {
            instr->count_references(ctx);
        }
        defaultCase->count_references(ctx);
    }
};

#endif
