#ifndef STEPBYSTEP_ASSUMEDINSTRUCTION_H
#define STEPBYSTEP_ASSUMEDINSTRUCTION_H

#include "Instruction.h"
#include "../../tree/ExpressionNode.h"

class AssumedInstruction : public Instruction
{
    std::string heuristicName;

    std::vector<ExpressionPtr> parameters;

    InstructionPtr instruction;

public:

    AssumedInstruction(std::string heuristicName, std::vector<ExpressionPtr> parameters, InstructionPtr instruction)
        : heuristicName {std::move(heuristicName)}, parameters {std::move(parameters)}, instruction {std::move(instruction)} {}

    void compile(Context& ctx) const override {
        ctx << Elem::OPENB;

        std::string declaration = "assumed_" + heuristicName + " assumptions {container";

        for (auto& parameter : parameters) {
            declaration += ", ";

            if (parameter) {
                declaration += parameter->to_pointer_string(ctx);
            } else {
                declaration += "{}";
            }
        }

        declaration += "}";

        ctx << declaration << Elem::END;

        ctx << Elem::WHILE << "auto tmp = assumptions.next_assumption()";

        std::string binding = "auto& [";

        for (size_t i = 0; i < parameters.size(); i++) {
            if (i != 0) {
                binding += ", ";
            }
            binding += "ref" + std::to_string(i);
        }

        std::vector<Type> reference_types { ctx.find_heuristic(heuristicName).parameterTypes };
        reference_types.push_back(ctx.find_heuristic(heuristicName).returnType);

        ctx.reference_types = reference_types;

        binding += "] = *tmp";

        ctx << binding << Elem::END;

        instruction->compile(ctx);

        ctx << Elem::CLOSE << Elem::CLOSE;
    }

    void count_references(Context& ctx) const override {
        for (auto& parameter : parameters) {
            if (parameter) {
                parameter->count_for_pointer(ctx);
            }
        }
        instruction->count_references(ctx);
    }
};

#endif
