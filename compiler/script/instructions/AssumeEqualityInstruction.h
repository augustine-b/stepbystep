#ifndef STEPBYSTEP_ASSUMEEQUALITYINSTRUCTION_H
#define STEPBYSTEP_ASSUMEEQUALITYINSTRUCTION_H

#include "Instruction.h"
#include "../../tree/ExpressionNode.h"

class AssumeEqualityInstruction : public Instruction
{
    ExpressionPtr lhs;

    ExpressionPtr rhs;

    bool value;

public:

    AssumeEqualityInstruction(ExpressionPtr lhs, ExpressionPtr rhs, bool value)
            : lhs(std::move(lhs)), rhs(std::move(rhs)), value(value) {};

    void compile(Context& ctx) const override {
        ctx << Elem::OPENB;

        ctx << Elem::EXPRESSION_PTR << "lhs" << lhs->to_pointer_string(ctx);
        ctx << Elem::EXPRESSION_PTR << "rhs" << rhs->to_pointer_string(ctx);

        std::string relation = value ? "==" : "!=";

        ctx << Elem::DEBUG << ("\"assumed: \"") << "lhs->to_string()"
            << ("\"" + relation + "\"") << "rhs->to_string()" << Elem::END;

        ctx << (std::string("assume_equals(lhs, rhs, ") + (value ? "1" : "-1") + ")") << Elem::END;

        ctx << Elem::CLOSE;
    }

    void count_references(Context& ctx) const override {
        lhs->count_for_pointer(ctx);
        rhs->count_for_pointer(ctx);
    }
};

#endif
