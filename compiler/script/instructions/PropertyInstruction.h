#ifndef CONDITIONALINSTRUCTION_H
#define CONDITIONALINSTRUCTION_H

#include <iostream>
#include "LogicalInstruction.h"
#include "../../keywords/Property.h"
#include "../../tree/ExpressionNode.h"

class PropertyInstruction : public LogicalInstruction
{
    ExpressionPtr lhs;

    Property rhs;

    bool value;

public:

    PropertyInstruction(ExpressionPtr lhs, Property rhs, bool value)
            : lhs(std::move(lhs)), rhs(std::move(rhs)), value(value) {};

    bool compile_propagated(Context& ctx) override {
        if (auto wildcard = lhs->is_wildcard()) {
            ctx << Elem::CONSTRAINT << wildcard->id;

            eval_property(ctx);

            ctx << Elem::RETURN << (value ? "actual" : "!actual");

            ctx << Elem::CLOSE_CONSTRAINT;
            return true;
        }

        return false;
    }

    void count_references(Context& ctx) const override {
        lhs->count_for_reference(ctx);
    }

protected:

    void build(Context& ctx, bool flip, const std::function<void()>& fn) const override {
        ctx << Elem::OPENB;
        ctx << Elem::EXPRESSION_REF << "built" << lhs->to_reference_string(ctx);

        eval_property(ctx);

        ctx << Elem::IF << (value ^ flip ? "actual" : "!actual");

        fn();

        ctx << Elem::CLOSE << Elem::CLOSE;
    }

private:

    void eval_property(Context& ctx) const {
        ctx << Elem::BOOL << "actual";

        if (rhs == Property::INTEGER) {
            ctx << "built.is_integer()";
        } else if (rhs == Property::NUMERIC) {
            ctx << "built.is_numeric()";
        } else if (rhs == Property::CONSTANT) {
            ctx << "built.is_constant()";
        } else if (rhs == Property::EVEN) {
            ctx << "built.is_even()";
        } else {
            std::cerr << "Unhandled property: " << rhs.name << std::endl;
            return;
        }

        ctx << Elem::DEBUG << ("\"checking: " + rhs.name + " property of \"")
            << "built.to_string()" << "\" : \"" << "actual" << Elem::END;
    }
};

#endif
