#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include <memory>
#include "../CodeBuilder.h"
#include "../Context.h"

class Instruction
{
public:

    virtual ~Instruction() = default;

    virtual void compile(Context& ctx) const = 0;

    virtual bool compile_propagated(Context&) { return false; }

    virtual void count_references(Context& ctx) const = 0;
};

using InstructionPtr = std::unique_ptr<Instruction>;

#endif
