#ifndef STEPBYSTEP_PROVEINEQUALITYINSTRUCTION_H
#define STEPBYSTEP_PROVEINEQUALITYINSTRUCTION_H

#include "LogicalInstruction.h"
#include "../../tree/ExpressionNode.h"

class ProveInequalityInstruction : public LogicalInstruction
{
    bool less;

    bool strict;

    ExpressionPtr lhs;

    ExpressionPtr rhs;

public:

    ProveInequalityInstruction(std::pair<bool, bool> cmp, ExpressionPtr lhs, ExpressionPtr rhs)
            : less {cmp.first}, strict {cmp.second}, lhs {std::move(lhs)}, rhs {std::move(rhs)} {}

    bool compile_propagated(Context& ctx) override {
        if (lhs->is_wildcard() && rhs->is_simple()) {
            build_constraint(ctx, *lhs, *rhs, false);
            return true;
        }

        if (rhs->is_wildcard() && lhs->is_simple()) {
            build_constraint(ctx, *rhs, *lhs, true);
            return true;
        }

        return false;
    }

    void count_references(Context& ctx) const override {
        lhs->count_for_reference(ctx);
        rhs->count_for_reference(ctx);
    }

protected:

    void build(Context& ctx, bool flip, const std::function<void()> &fn) const override {
        if (lhs->is_zero()) {
            build_prove_cmp_zero(ctx, *rhs, flip, strict ? (less ? 2 : -2) : (less ? 1 : -1), fn);
            return;
        } else if (rhs->is_zero()) {
            build_prove_cmp_zero(ctx, *lhs, flip, strict ? (less ? -2 : 2) : (less ? -1 : 1), fn);
            return;
        }

        ctx << Elem::OPENB;

        ctx << Elem::EXPRESSION_REF << "lhs" << lhs->to_reference_string(ctx);
        ctx << Elem::EXPRESSION_REF << "rhs" << rhs->to_reference_string(ctx);

        ctx << Elem::INT << "actual" << "prove_compare(lhs, rhs)";

        ctx << Elem::DEBUG << ("\"proving: comparison between \"") << "lhs.to_string()"
            << "\" and \"" << "rhs.to_string()" << "\" : \"" << "actual" << Elem::END;

        ctx << Elem::IF;

        std::string tmp;
        tmp += "actual";
        tmp += flip ? " != " : " == ";
        tmp += strict ? (less ? "-2" : "2") : (less ? "-1" : "1");

        ctx << tmp;

        fn();

        ctx << Elem::CLOSE << Elem::CLOSE;
    }

    static void build_prove_cmp_zero(Context& ctx, ExpressionRef expr, bool flip, int val,
                                     const std::function<void()>& fn) {
        ctx << Elem::OPENB;

        ctx << Elem::EXPRESSION_REF << "built" << expr.to_reference_string(ctx);

        ctx << Elem::INT << "actual" << "prove_sgn(built)";

        ctx << Elem::DEBUG << ("\"proving: sign of \"") << "built.to_string()"
            << "\" : \"" << "actual" << Elem::END;

        ctx << Elem::IF;

        std::string tmp;
        tmp += "actual";
        tmp += flip ? " != " : " == ";
        tmp += std::to_string(val);

        ctx << tmp;

        fn();

        ctx << Elem::CLOSE << Elem::CLOSE;
    }

private:

    void build_constraint(Context& ctx, ExpressionRef wildcard, ExpressionRef expression, bool flip) {
        if (expression.is_zero()) {
            build_constraint_cmp_zero(ctx, wildcard, flip);
            return;
        }

        ctx << Elem::CONSTRAINT << static_cast<const WildcardNode&>(wildcard).id;

        ctx << Elem::EXPRESSION_REF << "expression" << expression.to_reference_string(ctx);

        ctx << Elem::INT << "actual" << "prove_compare(built, expression)";

        ctx << Elem::DEBUG << ("\"proving: comparison between \"") << "built.to_string()"
            << ("\" and \"") << "expression.to_string()" << "\" : \"" << "actual" << Elem::END;

        ctx << Elem::RETURN << (std::string {"actual == "} + (less ^ flip ? (strict ? "-2" : "-1") : (strict ? "2" : "1")));

        ctx << Elem::CLOSE_CONSTRAINT;
    }

    void build_constraint_cmp_zero(Context& ctx, ExpressionRef wildcard, bool flip) {
        ctx << Elem::CONSTRAINT << static_cast<const WildcardNode&>(wildcard).id;

        std::string relation = std::string(less ^ flip ? "<" : ">") + (strict ? "" : "=");

        ctx << Elem::INT << "actual" << "prove_sgn(built)";

        ctx << Elem::DEBUG << ("\"proving: sign of \"") << "built.to_string()"
            << "\" : \"" << "actual" << Elem::END;

        ctx << Elem::RETURN << (std::string {"actual == "} + (less ^ flip ? (strict ? "-2" : "-1") : (strict ? "2" : "1")));

        ctx << Elem::CLOSE_CONSTRAINT;
    }
};

#endif
