#ifndef STEPBYSTEP_ASSUMEPROPERTYINSTRUCTION_H
#define STEPBYSTEP_ASSUMEPROPERTYINSTRUCTION_H

#include "LogicalInstruction.h"
#include "../../keywords/Property.h"
#include "../../tree/ExpressionNode.h"

class AssumePropertyInstruction : public Instruction
{
    ExpressionPtr lhs;

    Property rhs;

    bool value;

public:

    AssumePropertyInstruction(ExpressionPtr lhs, Property rhs, bool value)
            : lhs(std::move(lhs)), rhs(std::move(rhs)), value(value) {};

    void compile(Context& ctx) const override {
        ctx << Elem::OPENB;
        ctx << Elem::EXPRESSION_PTR << "built" << lhs->to_pointer_string(ctx);

        ctx << Elem::DEBUG << ("\"assumed: " + rhs.name + " property of \"")
            << "built->to_string()" << Elem::END;

        if (rhs == Property::INTEGER) {
            ctx << "assume_integer(built)" << Elem::END;
        } else {
            std::cerr << "No proof exists for property: " << rhs.name << " therefore it cannot be assumed" << std::endl;
            return;
        }

        ctx << Elem::CLOSE;
    }

    void count_references(Context& ctx) const override {
        lhs->count_for_pointer(ctx);
    }
};

#endif
