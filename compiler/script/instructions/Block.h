#ifndef STEPBYSTEP_BLOCK_H
#define STEPBYSTEP_BLOCK_H

#include "Instruction.h"

class Block : public Instruction
{
    std::vector<InstructionPtr> instructions;

public:

    explicit Block() : instructions {} {}

    void add_instruction(InstructionPtr instruction) {
        instructions.push_back(std::move(instruction));
    }

    void compile(Context& ctx) const override {
        for(const auto& instruction : instructions) {
            instruction->compile(ctx);
        }
    }

    void count_references(Context& ctx) const override {
        for (auto& instr : instructions) {
            instr->count_references(ctx);
        }
    }
};

#endif
