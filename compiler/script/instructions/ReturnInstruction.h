#ifndef RETURNINSTRUCTION_H
#define RETURNINSTRUCTION_H

#include "../Ruleset.h"
#include "Instruction.h"
#include "../../tree/ExpressionNode.h"


class ReturnInstruction : public Instruction
{
    std::vector<ExpressionPtr> values;

    std::optional<bool> on_r {};

    bool in_heuristic {false};

public:

    explicit ReturnInstruction(ExpressionPtr value, bool in_heuristic)
            : values {}, in_heuristic {in_heuristic} {
        values.push_back(std::move(value));
    }

    explicit ReturnInstruction(std::vector<ExpressionPtr> values, bool in_heuristic)
            : values {std::move(values)}, in_heuristic {in_heuristic} {};

    explicit ReturnInstruction(bool on_r)
            : values {}, on_r {on_r} {};

    void compile(Context& ctx) const override {
        if (on_r) {
            if (*on_r) {
                ctx << Elem::THROW << "\"expression is undefined on R\"";
            } else {
                ctx << Elem::THROW << "\"expression is undefined\"";
            }
        } else {
            if (in_heuristic) {
                ctx << Elem::RETURN << values[0]->coerce_reference(ctx, Type::INTEGER);
                return;
            }

            ctx << "std::vector<ExpressionPtr> values" << Elem::END;

            for (const auto& value : values) {
                ctx << "values.push_back(" + value->to_pointer_string(ctx) + ")" << Elem::END;
            }

            ctx << Elem::RETURN << ("{ Change {match->completeMatch, std::move(values)} }");
        }
    }

    void count_references(Context& ctx) const override {
        if (!on_r) {
            for (const auto& value : values) {
                value->count_for_pointer(ctx);
            }
        }
    }
};

#endif
