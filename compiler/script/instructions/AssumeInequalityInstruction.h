#ifndef STEPBYSTEP_ASSUMEINEQUALITYINSTRUCTION_H
#define STEPBYSTEP_ASSUMEINEQUALITYINSTRUCTION_H

#include <string>
#include "Instruction.h"
#include "../../tree/ExpressionNode.h"

class AssumeInequalityInstruction : public Instruction
{
    bool less;

    bool strict;

    ExpressionPtr lhs;

    ExpressionPtr rhs;

public:

    AssumeInequalityInstruction(std::pair<bool, bool> cmp, ExpressionPtr lhs, ExpressionPtr rhs)
            : less {cmp.first}, strict {cmp.second}, lhs {std::move(lhs)}, rhs {std::move(rhs)} {}

    void compile(Context& ctx) const override {
        if (lhs->is_zero()) {
            build_assumption_cmp_zero(ctx, *rhs, strict ? (less ? 2 : -2) : (less ? 1 : -1));
            return;
        } else if (rhs->is_zero()) {
            build_assumption_cmp_zero(ctx, *lhs, strict ? (less ? -2 : 2) : (less ? -1 : 1));
            return;
        }

        ctx << Elem::OPENB;

        ctx << Elem::EXPRESSION_PTR << "lhs" << lhs->to_pointer_string(ctx);
        ctx << Elem::EXPRESSION_PTR << "rhs" << rhs->to_pointer_string(ctx);

        std::string relation = std::string(less ? "<" : ">") + (strict ? "" : "=");

        ctx << Elem::DEBUG << ("\"assumed: \"") << "lhs->to_string()"
            << ("\"" + relation + "\"") << "rhs->to_string()" << Elem::END;

        std::string value = strict ? (less ? "-2" : "2") : (less ? "-1" : "1");

        ctx << ("assume_compare(lhs, rhs, " + value + ")") << Elem::END;

        ctx << Elem::CLOSE;
    }

    void count_references(Context& ctx) const override {
        lhs->count_for_pointer(ctx);
        rhs->count_for_pointer(ctx);
    }

protected:

    static void build_assumption_cmp_zero(Context& ctx, ExpressionRef expr, int val) {
        ctx << Elem::OPENB;

        ctx << Elem::EXPRESSION_PTR << "built" << expr.to_pointer_string(ctx);

        ctx << Elem::DEBUG << ("\"assumed: sign of\"") << "built->to_string()"
            << "\" : \"" << std::to_string(val) << Elem::END;

        ctx << ("assume_sgn(std::move(built), " + std::to_string(val) + ")") << Elem::END;

        ctx << Elem::CLOSE;
    }
};


#endif
