#ifndef RULE_H
#define RULE_H

#include <vector>
#include <memory>
#include "instructions/Instruction.h"
#include "../tree/ExpressionNode.h"
#include "../tree/OperatorNode.h"
#include "CodeBuilder.h"

class Rule
{
public:

    std::string name;

    ExpressionPtr pattern {};

    std::vector<InstructionPtr> instructions {};

    bool persistent {false};

    bool collapse {false};

    bool transparent {false};

    std::vector<std::string> skipifs {};

    Rule() : name() {}

    explicit Rule(std::string name) : name(std::move(name)) {}

    friend std::ostream& operator<<(std::ostream& out, const Rule& ruleset);

    void get_declaration(Context& ctx) const {
        ctx << ("std::optional<Change> " + name + "(ExpressionRef en)") << Elem::END;
    }

    void get_definition(Context& ctx) const {
        ctx << ("std::optional<Change> " + name + "(ExpressionRef en)") << Elem::OPEN;

        bool trivial_pattern {false};

        if (pattern->is_trivial_pattern()) {
            const auto& operator_p = static_cast<const OperatorNode&>(*pattern);

            std::string declaration = "TrivialMatcher matcher {Operator::" + operator_p.op.name + ", "
                       + operator_p.operands[0]->to_pattern_string() + ", "
                       + operator_p.operands[1]->to_pattern_string() + ", "
                       + "en}";

            ctx << declaration << Elem::END << Elem::SKIP;

            trivial_pattern = true;
        } else if (pattern->is_simple_pattern()) {
            ctx << ("SimpleMatcher matcher {" + pattern->to_pattern_string() + ", en}") << Elem::END << Elem::SKIP;
        } else if (pattern->complex_pattern_count() == 1) {
            ctx << ("ComplexMatcher matcher {" + pattern->to_pattern_string() + ", en}") << Elem::END << Elem::SKIP;
        } else {
            ctx << ("GeneralMatcher matcher {" + pattern->to_pattern_string() + ", en}") << Elem::END << Elem::SKIP;
        }

        size_t counter {0};

        if (!trivial_pattern) {
            while (counter < instructions.size() && instructions[counter]->compile_propagated(ctx)) {
                counter++;
            }
        }

        ctx.reset_refs();

        for (auto& instruction : instructions) {
            instruction->count_references(ctx);
        }

        ctx << Elem::WHILE << "auto match = matcher.next_match()";

        while (counter < instructions.size()) {
            instructions[counter]->compile(ctx);
            counter++;
        }

        ctx << Elem::CLOSE;
        ctx << Elem::RETURN << "{}";
        ctx << Elem::CLOSE;
    }

    void add_instruction(InstructionPtr instruction) {
        instructions.push_back(std::move(instruction));
    }
};

#endif