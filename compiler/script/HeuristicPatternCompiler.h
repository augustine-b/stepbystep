#ifndef STEPBYSTEP_HEURISTICPATTERNCOMPILER_H
#define STEPBYSTEP_HEURISTICPATTERNCOMPILER_H

#include <bitset>
#include "Context.h"
#include "../tree/ExpressionNode.h"
#include "../tree/WildcardNode.h"
#include "../tree/OperatorNode.h"
#include "../tree/SpecialWildcardNode.h"

inline void compile_pattern(Context& ctx, ExpressionRef pattern, const std::string& current_name, int& index, std::bitset<10>& used);

inline void compile_unary(Context& ctx, const OperatorNode& unary, const std::string& current_name, int& index, std::bitset<10>& used) {
    std::string new_name = "tmp" + std::to_string(index);
    ctx << Elem::EXPRESSION_REF << new_name << ("*static_cast<const OperatorNode&>(" + current_name + ").operands[0]");
    index++;

    compile_pattern(ctx, *unary.operands[0], new_name, index, used);
}

inline void compile_binary(Context& ctx, const OperatorNode& binary, const std::string& current_name, int& index, std::bitset<10>& used) {
    std::string new_name = "tmp" + std::to_string(index);
    ctx << Elem::EXPRESSION_REF << new_name << ("*static_cast<const OperatorNode&>(" + current_name + ").operands[0]");
    index++;

    compile_pattern(ctx, *binary.operands[0], new_name, index, used);

    new_name = "tmp" + std::to_string(index);
    ctx << Elem::EXPRESSION_REF << new_name << ("*static_cast<const OperatorNode&>(" + current_name + ").operands[1]");
    index++;

    compile_pattern(ctx, *binary.operands[1], new_name, index, used);
}

inline void compile_nary(Context& ctx, const OperatorNode& nary, const std::string& current_name, int& index, std::bitset<10>& used) {
    if (auto wildcard = nary.operands[0]->is_wildcard()) {
        if (auto swildcard = nary.operands[1]->is_special_wildcard()) {
            ctx << Elem::EXPRESSION_REF << ("value" + std::to_string(wildcard->id))
                << ("*static_cast<const OperatorNode&>(" + current_name + ").operands[0]");

            ctx << Elem::EXPRESSION_PTR << ("svalue" + std::to_string(swildcard->id))
                << ("util::create_rest(static_cast<const OperatorNode&>(" + current_name + "), {0})");

            return;
        }
    }

    std::cerr << "Could not interpret pattern in heuristic" << std::endl;
}

inline void compile_pattern(Context& ctx, ExpressionRef pattern, const std::string& current_name, int& index, std::bitset<10>& used) {
    if (auto wildcard = pattern.is_wildcard()) {
        if (used[wildcard->id]) {
            ctx << Elem::IF << ("value" + std::to_string(wildcard->id) + " != " + current_name);
            ctx << Elem::CONTINUE;
            ctx << Elem::CLOSE;
        } else {
            ctx << Elem::EXPRESSION_REF << ("value" + std::to_string(wildcard->id)) << current_name;
            used[wildcard->id] = true;
        }
    } else if (auto operation = pattern.is_operation()) {
        Operator op = operation->op;

        ctx << Elem::IF << ("!" + current_name + ".is_operation(Operator::" + op.name + ")") << Elem::CONTINUE << Elem::CLOSE;

        if (op == Operator::MINUS || op == Operator::ABS) {
            compile_unary(ctx, *operation, current_name, index, used);
        } else if (op == Operator::DIVIDE || op == Operator::POWER) {
            compile_binary(ctx, *operation, current_name, index, used);
        } else if (op == Operator::PLUS || op == Operator::MULTIPLY) {
            compile_nary(ctx, *operation, current_name, index, used);
        } else {
            std::cerr << "Could not interpret pattern in heuristic" << std::endl;
        }
    } else {
        std::cerr << "Could not interpret pattern in heuristic" << std::endl;
    }
}

inline void compile_pattern(Context& ctx, const std::vector<ExpressionPtr>& patterns) {
    int index {0};
    std::bitset<10> used {};

    for (size_t i = 0; i < patterns.size(); i++) {
        compile_pattern(ctx, *patterns[i], std::string {static_cast<char>('a' + i)}, index, used);
    }
}

#endif
