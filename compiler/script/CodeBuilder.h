#ifndef STEPBYSTEP_LINE_H
#define STEPBYSTEP_LINE_H

#include <sstream>
#include "../keywords/Type.h"

enum class Elem {
    NAMESPACE,
    BITSET,
    RULE_ARRAY,
    BITSET_ARRAY,
    ARRAY,
    EXPRESSION_PTR,
    EXPRESSION_REF,
    BOOL,
    INT,
    DOUBLE,
    STRUCTURED_BINDING,
    IF,
    WHILE,
    ELSE,
    CONTINUE,
    RETURN,
    THROW,
    OPEN,
    OPENB,
    CLOSE,
    GOTO,
    END,
    SKIP,
    CONSTRAINT,
    CLOSE_CONSTRAINT,
    DEBUG,
};

class CodeBuilder
{
public:

    explicit CodeBuilder(std::string_view header)
        : code{}, lastElem{}, indent{0}, first{false}
    {
        code << header;
    }

    CodeBuilder& operator<<(Elem elem) {
        switch (elem) {
            case Elem::END:
                if (lastElem == Elem::DEBUG) {
                    code << ")";
                }

                code << ";";

            [[fallthrough]];
            case Elem::SKIP:
                code << "\n";
                lastElem = elem;
                return *this;

            case Elem::OPEN:
                code << " {\n";
                indent++;
                lastElem = elem;
                return *this;

            default:
                break;
        }

        if (lastElem == Elem::CLOSE && elem != Elem::CLOSE) {
            code << '\n';
        }

        if (elem == Elem::CLOSE) {
            if (lastElem == Elem::RULE_ARRAY || lastElem == Elem::ARRAY) {
                indent -= 2;
            } else {
                indent--;
            }
        }

        if (elem == Elem::CLOSE_CONSTRAINT) {
            indent -= 2;
        }

        if (elem == Elem::ELSE) {
            for (int i = 0; i < indent - 1; i++) {
                code << '\t';
            }
            code << "} else {\n";
            return *this;
        }

        for (int i = 0; i < indent; i++) {
            code << '\t';
        }

        switch (elem) {
            case Elem::NAMESPACE:
                code << "namespace ";
                break;

            case Elem::RULE_ARRAY:
                code << "std::optional<Change> (*";
                break;

            case Elem::BITSET:
                code << "constexpr immutable_bitset ";
                break;

            case Elem::BITSET_ARRAY:
                code << "constexpr immutable_bitset<";
                break;

            case Elem::EXPRESSION_PTR:
                code << "ExpressionPtr ";
                break;

            case Elem::EXPRESSION_REF:
                code << "ExpressionRef ";
                break;

            case Elem::BOOL:
                code << "bool ";
                break;

            case Elem::INT:
                code << "int ";
                break;

            case Elem::DOUBLE:
                code << "double ";
                break;

            case Elem::STRUCTURED_BINDING:
                code << "auto [";
                break;

            case Elem::WHILE:
                code << "while (";
                break;

            case Elem::IF:
                code << "if (";
                break;

            case Elem::RETURN:
                code << "return ";
                break;

            case Elem::THROW:
                code << "throw ";
                break;

            case Elem::DEBUG:
                code << "DEBUG(";
                break;

            case Elem::CONTINUE:
                code << "continue;\n";
                break;

            case Elem::GOTO:
                code << "goto ";
                break;

            case Elem::OPENB:
                code << "{\n";
                indent++;
                break;

            case Elem::CONSTRAINT:
                code << "matcher.constraints";
                break;

            case Elem::CLOSE_CONSTRAINT:
                code << "};\n\n";
                break;

            case Elem::CLOSE:
                if (lastElem == Elem::RULE_ARRAY || lastElem == Elem::BITSET || lastElem == Elem::ARRAY) {
                    code << "};\n";
                } else {
                    code << "}\n";
                }

                break;

            default:
                break;
        }

        first = true;
        lastElem = elem;

        return *this;
    }

    CodeBuilder& operator<<(const std::string_view& text) {
        if (first) {
            first = false;

            switch (lastElem) {
                case Elem::NAMESPACE:
                    code << text << "\n{\n";
                    indent++;
                    return *this;
                    
                case Elem::RULE_ARRAY:
                    code << text << "[])(ExpressionRef) = {\n";
                    indent += 2;
                    return *this;

                case Elem::BITSET:
                    code << text << " {";
                    return *this;

                case Elem::BITSET_ARRAY:
                    code << text << "> ";
                    lastElem = Elem::ARRAY;
                    first = true;
                    return *this;

                case Elem::ARRAY:
                    code << text << "[] = {\n";
                    indent += 2;
                    return *this;

                case Elem::EXPRESSION_PTR:
                case Elem::EXPRESSION_REF:
                case Elem::BOOL:
                case Elem::INT:
                case Elem::DOUBLE:
                    code << text << " = ";
                    return *this;

                case Elem::STRUCTURED_BINDING:
                    code << text << "] = ";
                    return *this;

                case Elem::IF:
                case Elem::WHILE:
                    code << text << ") {\n";
                    indent++;
                    return *this;

                case Elem::GOTO:
                case Elem::RETURN:
                case Elem::THROW:
                    code << text << ";\n";
                    return *this;

                case Elem::CONSTRAINT:
                    code << "[" << text << "] = [](ExpressionRef built) {\n";
                    indent += 2;
                    return *this;

                case Elem::DEBUG:
                    code << text;
                    return *this;

                default:
                    break;
            }
        }

        if (lastElem == Elem::CLOSE) {
            code << "\n";
        }

        if (lastElem == Elem::DEBUG) {
            code << " << " << text;
            return *this;
        }

        if (lastElem == Elem::EXPRESSION_PTR || lastElem == Elem::EXPRESSION_REF
                || lastElem == Elem::BOOL || lastElem == Elem::INT || lastElem == Elem::DOUBLE
                || lastElem == Elem::STRUCTURED_BINDING) {
            code << text << ";\n";
            lastElem = Elem::END;
            return *this;
        }

        if (lastElem == Elem::BITSET) {
            code << text << "};\n";
            lastElem = Elem::END;
            return *this;
        }

        for (int i = 0; i < indent; i++) {
            code << '\t';
        }

        if (lastElem == Elem::RULE_ARRAY || lastElem == Elem::ARRAY) {
            code << text << ",\n";
            return *this;
        }

        code << text;

        return *this;
    }


    CodeBuilder& operator<<(int number) {
        return *this << std::to_string(number);
    }

    CodeBuilder& operator<<(const Type& type) {
        if (type == Type::INTEGER) {
            return *this << Elem::INT;
        } else if (type == Type::DECIMAL) {
            return *this << Elem::DOUBLE;
        } else if (type == Type::LOGICAL) {
            return *this << Elem::BOOL;
        } else if (type == Type::EXPRESSION) {
            return *this << Elem::EXPRESSION_REF;
        }
    }

    std::string str() {
        return code.str();
    }

private:

    std::ostringstream code;

    Elem lastElem;

    int indent;

    bool first;
};

#endif
