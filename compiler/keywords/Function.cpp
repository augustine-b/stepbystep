#include "Function.h"

const Function Function::EVAL {1, "eval", 1, {Type::DECIMAL, Type::EXPRESSION}};

const Function Function::GCD {2, "gcd", 2, {Type::INTEGER, Type::EXPRESSION}};

const Function Function::SPLIT_INTEGER {3, "split_integer", 1, {Type::NO_TYPE, Type::NO_TYPE}};

const Function Function::ROUND {4, "round", 1, {Type::INTEGER, Type::EXPRESSION}};

const Function Function::NO_FUNCTION {0, "no function", -1, {Type::NO_TYPE, Type::NO_TYPE}};

const Function Function::functions[] {
        EVAL,
        GCD,
        SPLIT_INTEGER,
        ROUND,
};