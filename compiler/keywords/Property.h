#include <utility>

#ifndef PROPERTY_H
#define PROPERTY_H

#include <string>
#include <iostream>

class Property
{
public:

    static const Property NO_PROPERTY;

    static const Property INTEGER;

    static const Property NUMERIC;

    static const Property CONSTANT;

    static const Property EVEN;

    static const Property properties[4];

    Property(int id, std::string name)
        : name(std::move(name)), id(id) {};

    static Property by_name(const std::string& name) {
        for (const Property& property : properties) {
            if (property.name == name) {
                return property;
            }
        }

        throw "Could not find property named " + name;
    }

    bool operator==(const Property& other) const {
        return id == other.id;
    }

    const std::string name;

private:

    const int id;
};

#endif
