#include "../keywords/Property.h"

const Property Property::NO_PROPERTY {0, "no propery"};

const Property Property::INTEGER {1, "integer"};

const Property Property::NUMERIC {2, "numeric"};

const Property Property::CONSTANT {3, "constant"};

const Property Property::EVEN {4, "even"};

const Property Property::properties[] {
        INTEGER,
        NUMERIC,
        CONSTANT,
        EVEN,
};