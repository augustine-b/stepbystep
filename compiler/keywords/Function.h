#ifndef FUNCTION_H
#define FUNCTION_H

#include <string>
#include <iostream>
#include "Type.h"

class Function
{
    int id;

    Function(int id, std::string name, int arity, std::pair<const Type&, const Type&> accepted_types)
            : id {id}, name {std::move(name)}, arity {arity}, accepted_types {accepted_types} {};

public:

    static const Function EVAL;

    static const Function GCD;

    static const Function SPLIT_INTEGER;

    static const Function ROUND;

    static const Function NO_FUNCTION;

    static const Function functions[4];

    const std::string name;

    const int arity;

    std::pair<const Type&, const Type&> accepted_types;

    static Function by_name(const std::string& name) {
        for (const Function& function : functions) {
            if (function.name == name) {
                return function;
            }
        }

        throw "Could not find function named " + name;
    }

    bool operator==(const Function& other) const {
        return id == other.id;
    }
};

#endif
