#include "Type.h"

const Type Type::NO_TYPE {};

const Type Type::LOGICAL {1, "logical", "bool", "bool", "std::optional<bool>"};

const Type Type::INTEGER {2, "int", "int", "int", "std::optional<int>"};

const Type Type::DECIMAL {3, "decimal", "double", "double", "std::optional<double>"};

const Type Type::EXPRESSION {4, "expression", "ExpressionPtr", "ExpressionRef", "ExpressionPtr", true};

const Type Type::types[] {
        LOGICAL,
        DECIMAL,
        INTEGER,
        EXPRESSION,
};