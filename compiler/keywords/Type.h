#ifndef STEPBYSTEP_PARAMETERTYPE_H
#define STEPBYSTEP_PARAMETERTYPE_H

#include <string>

class Type
{
public:

    static const Type NO_TYPE;

    static const Type EXPRESSION;

    static const Type INTEGER;

    static const Type DECIMAL;

    static const Type LOGICAL;

    static const Type types[4];

    int id {};

    std::string name {"no type"};

    std::string ptrname {};
    std::string refname {};
    std::string optname {};

    bool needs_deref {false};

    static Type by_name(const std::string& name) {
        for (const Type& type : types) {
            if (type.name == name) {
                return type;
            }
        }

        throw "Could not find type named " + name;
    }

    bool operator==(const Type& other) const {
        return id == other.id;
    }

    bool operator!=(const Type& other) const {
        return id == other.id;
    }

    bool operator<=(const Type& other) const {
        return id <= other.id;
    }
};

#endif
