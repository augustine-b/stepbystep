#include "Operator.h"

const Operator Operator::EQUALS {1, 0, 2, true, "eq", "EQUALS"};

const Operator Operator::PLUS {2, 1, -1, false, "+", "PLUS"};
const Operator Operator::MINUS {3, 1, 1, false, "-", "MINUS"};
const Operator Operator::PLUSMINUS {4, 1, 1, true, "plusminus", "PLUSMINUS"};

const Operator Operator::MULTIPLY {5, 2, -1, false, "*", "MULTIPLY"};
const Operator Operator::DIVIDE {6, 2, 2, false, "/", "DIVIDE"};

const Operator Operator::POWER {7, 3, 2, false, "^", "POWER"};

const Operator Operator::NROOT {8, 3, 2, true, "nroot", "NROOT"};
const Operator Operator::LOG {9, 3, 2, true, "log", "LOG"};
const Operator Operator::ABS {10, 3, 1, true, "abs", "ABS"};

const Operator Operator::operators[] = {
        PLUS, MINUS, PLUSMINUS, MULTIPLY, DIVIDE, POWER, NROOT, LOG, ABS,
};