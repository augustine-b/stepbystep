#ifndef OPERATOR_H
#define OPERATOR_H

#include <string>

class Operator
{
public:

    static const Operator EQUALS;

    static const Operator PLUS;
    static const Operator MINUS;
    static const Operator PLUSMINUS;

    static const Operator MULTIPLY;
    static const Operator DIVIDE;

    static const Operator POWER;

    static const Operator NROOT;
    static const Operator LOG;
    static const Operator ABS;

    static const Operator operators[9];

    int priority;
    int arity;
    bool function;
    std::string str;
    std::string name;

    bool operator==(const Operator& other) const {
        return id == other.id;
    }

    bool operator!=(const Operator& other) const {
        return id != other.id;
    }

    static Operator get(const std::string& c) {
        for (Operator op : operators) {
            if (op.str == c) {
                return op;
            }
        }
    }

private:
    const int id;

    Operator(int id, int priority, int arity, bool function, std::string str, std::string name)
        : priority {priority}, arity {arity}, function {function},
            str {std::move(str)}, name {std::move(name)}, id {id} {};
};


#endif
