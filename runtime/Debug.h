#ifndef THESIS_DEBUG_H
#define THESIS_DEBUG_H

#ifdef NDEBUG
#define DEBUG(str) do { } while (false)
#else
#define DEBUG(str) do { std::cout << str << std::endl; } while(false)
#endif

#endif

