#ifndef STEPBYSTEP_SIMPLEMATCHER_H
#define STEPBYSTEP_SIMPLEMATCHER_H

#include <deque>
#include <optional>
#include "../tree/ExpressionNode.h"
#include "../tree/OperatorNode.h"
#include "../tree/WildcardNode.h"
#include "../tree/SpecialWildcardNode.h"

struct SimpleMatch
{
    ExpressionRef completeMatch;

    std::array<ExpressionAddr, 10> wildcards;

    explicit SimpleMatch(ExpressionRef completeMatch)
            : completeMatch {completeMatch}, wildcards {} {}
};

class SimpleMatcher
{
    ExpressionPtr pattern;

    std::deque<std::reference_wrapper<const ExpressionNode>> queue;

public:

    SimpleMatcher(ExpressionPtr pattern, ExpressionRef base)
            : pattern {std::move(pattern)}, queue {} {
        queue.emplace_back(base);
    };

    std::optional<SimpleMatch> next_match() {
        while (!queue.empty()) {
            ExpressionRef current = queue[0];
            queue.pop_front();

            if (current.is_operation()) {
                for (auto const& op : static_cast<const OperatorNode&>(current).operands) {
                    queue.emplace_back(*op);
                }
            }

            SimpleMatch match {current};

            if (do_match(current, *pattern, match)) {
                return { match };
            }
        }

        return std::nullopt;
    }

private:

    bool do_match(ExpressionRef current_b, ExpressionRef current_p, SimpleMatch& match) {
        if (current_b.is_operation() && current_p.is_operation()) {
            const auto& operator_b = static_cast<const OperatorNode&>(current_b);
            const auto& operator_p = static_cast<const OperatorNode&>(current_p);

            if (operator_b.op == operator_p.op
                    && operator_b.operands.size() == operator_p.operands.size()) {
                for (size_t i = 0; i < operator_b.operands.size(); i++) {
                    if (!do_match(*operator_b.operands[i], *operator_p.operands[i], match)) {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        if (current_p.is_wildcard()) {
            const auto& wildcard_p = static_cast<const WildcardNode&>(current_p);

            if (match.wildcards[wildcard_p.id]) {
                return *match.wildcards[wildcard_p.id] == current_b;
            } else {
                match.wildcards[wildcard_p.id] = &current_b;
                return true;
            }
        }

        return current_b == current_p;
    }
};

#endif
