#include <utility>

#ifndef STEPBYSTEP_TRIVIALMATCHER_H
#define STEPBYSTEP_TRIVIALMATCHER_H

#include <deque>
#include <optional>
#include "../tree/ExpressionNode.h"
#include "../tree/OperatorNode.h"
#include "../tree/WildcardNode.h"
#include "../tree/SpecialWildcardNode.h"
#include "GeneralMatcher.h"

struct TrivialMatch
{
    ExpressionRef completeMatch;

    std::array<ExpressionAddr, 2> wildcards;

    explicit TrivialMatch(ExpressionRef completeMatch)
            : completeMatch {completeMatch}, wildcards {} {}
};

class TrivialMatcher
{
    Operator op;

    ExpressionPtr lhs;

    ExpressionPtr rhs;

    std::deque<std::reference_wrapper<const ExpressionNode>> queue;

public:

    TrivialMatcher(Operator op, ExpressionPtr lhs, ExpressionPtr rhs, ExpressionRef base)
            : op {std::move(op)}, lhs {std::move(lhs)}, rhs {std::move(rhs)}, queue {} {
        queue.emplace_back(base);
    };

    std::optional<TrivialMatch> next_match() {
        while (!queue.empty()) {
            ExpressionRef current = queue[0];
            queue.pop_front();

            if (current.is_operation()) {
                for (auto const& operand : static_cast<const OperatorNode&>(current).operands) {
                    queue.emplace_back(*operand);
                }
            }

            TrivialMatch match {current};

            if (do_match(current, match)) {
                return { match };
            }
        }

        return {};
    }

private:

    bool do_match(ExpressionRef current_b, TrivialMatch& match) {
        if (current_b.is_operation()) {
            const auto& operator_b = static_cast<const OperatorNode&>(current_b);

            if (operator_b.op == op) {
                if (lhs->is_wildcard()) {
                    match.wildcards[static_cast<const WildcardNode&>(*lhs).id] = operator_b.operands[0].get();
                } else if (*lhs != *operator_b.operands[0]) {
                    return false;
                }

                if (rhs->is_wildcard()) {
                    int id = static_cast<const WildcardNode&>(*rhs).id;

                    if (match.wildcards[id] == nullptr) {
                        match.wildcards[id] = operator_b.operands[1].get();
                        return true;
                    } else {
                        return *match.wildcards[id] == *rhs;
                    }
                } else {
                    return *rhs == *operator_b.operands[1];
                }
            }
        }

        return false;
    }
};

#endif
