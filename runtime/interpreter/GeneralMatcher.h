#ifndef GENERAL_MATCHER_H
#define GENERAL_MATCHER_H

#include <deque>
#include <optional>
#include "../tree/ExpressionNode.h"
#include "../tree/OperatorNode.h"
#include "../tree/WildcardNode.h"
#include "../tree/SpecialWildcardNode.h"
#include "../utility/Functions.h"

struct GeneralMatch
{
    ExpressionRef completeMatch;

    std::array<ExpressionAddr, 10> wildcards;

    std::array<ExpressionPtr, 10> special_wildcards;

    explicit GeneralMatch(ExpressionRef completeMatch)
            : completeMatch {completeMatch}, wildcards {}, special_wildcards{} {}

    void null() {
        for (int i = 0; i < 10; i++) {
            wildcards[i] = nullptr;
        }
    }
};

class GeneralMatcher
{
    ExpressionPtr pattern;

    std::reference_wrapper<const ExpressionNode> current;

    std::deque<std::reference_wrapper<const ExpressionNode>> queue;

    std::vector<std::reference_wrapper<const ExpressionNode>> narys;

    std::vector<size_t> indices;

    std::vector<size_t> totals;

public:

    std::array<bool(*)(ExpressionRef), 10> constraints;

    GeneralMatcher(ExpressionPtr pattern, ExpressionRef base)
        : pattern {std::move(pattern)}, current {base}, queue {}, constraints {} {
        queue.emplace_back(base);
    };

    std::optional<GeneralMatch> next_match() {
        while (!is_exhausted()) {
            GeneralMatch match { current };
            if (do_match(current, *pattern, match)) {
                return { std::move(match) };
            }
        }

        while (!queue.empty()) {
            current = queue[0];
            queue.pop_front();

            if (current.get().is_operation()) {
                for (auto const& op : static_cast<const OperatorNode&>(current.get()).operands) {
                    queue.emplace_back(*op);
                }
            }

            GeneralMatch match {current};

            narys.clear();
            totals.clear();
            indices.clear();

            if (do_match(current, *pattern, match)) {
                return { std::move(match) };
            } else {
                while (!is_exhausted()) {
                    match.null();
                    if (do_match(current, *pattern, match)) {
                        return { std::move(match) };
                    }
                }
            }
        }

        return std::nullopt;
    }

private:

    bool do_match(ExpressionRef current_b, ExpressionRef current_p, GeneralMatch& match) {
        if (current_b.is_operation() && current_p.is_operation()) {
            const auto& operator_b = static_cast<const OperatorNode&>(current_b);
            const auto& operator_p = static_cast<const OperatorNode&>(current_p);

            if (operator_b.op == operator_p.op) {
                if (operator_b.op == Operator::PLUS || operator_b.op == Operator::MULTIPLY) {
                    return do_match_plus_multiply(operator_b, operator_p, match);
                } else if (operator_b.operands.size() == operator_p.operands.size()) {
                    for (size_t i = 0; i < operator_b.operands.size(); i++) {
                        if (!do_match(*operator_b.operands[i], *operator_p.operands[i], match)) {
                            return false;
                        }
                    }

                    return true;
                }
            }

            return false;
        }

        if (current_p.is_wildcard()) {
            const auto& wildcard_p = static_cast<const WildcardNode&>(current_p);

            if (match.wildcards[wildcard_p.id]) {
                return *match.wildcards[wildcard_p.id] == current_b;
            } else {
                if (constraints[wildcard_p.id] && !constraints[wildcard_p.id](current_b)) {
                    return false;
                }

                match.wildcards[wildcard_p.id] = &current_b;
                return true;
            }
        }

        return current_b == current_p;
    }

    bool do_match_plus_multiply(const OperatorNode& current, const OperatorNode& pattern, GeneralMatch& match) {
        bool hasRestCard = pattern.operands.back()->is_special_wildcard();

        if (!hasRestCard && pattern.operands.size() != current.operands.size()) {
            return false;
        }

        int nodeIndex = find(current);

        size_t passIndex;

        if (nodeIndex == -1) {
            narys.emplace_back(current);
            indices.push_back(0);
            totals.push_back(factorial(current.operands.size()));

            passIndex = 0;
        } else {
            passIndex = indices[nodeIndex];
        }

        if (hasRestCard) {
            int restId = static_cast<SpecialWildcardNode&>(*pattern.operands.back()).id;

            size_t n = current.operands.size();
            size_t k = pattern.operands.size() - 1;

            if (k > n) {
                return false;
            }

            size_t actualPass = passIndex / factorial(n - k);

            switch (k) {
                case 1: {
                    if (actualPass == n) {
                        return false;
                    }

                    bool success = do_match(*current.operands[actualPass], *pattern.operands[0], match);
                    if (success) {
                        match.special_wildcards[restId] = util::create_rest(current, {actualPass});
                        return true;
                    }

                    return false;
                }

                case 2: {
                    if (actualPass == n * (n - 1)) {
                        return false;
                    }

                    size_t first = actualPass / (n - 1);
                    size_t second = actualPass % (n - 1);

                    if (second >= first) {
                        second++;
                    }

                    bool success = do_match(*current.operands[first], *pattern.operands[0], match)
                              && do_match(*current.operands[second], *pattern.operands[1], match);

                    if (success) {
                        match.special_wildcards[restId] = util::create_rest(current, {first, second});
                        return true;
                    }

                    return false;
                }

                case 3: {
                    if (actualPass == n * (n - 1) * (n - 2)) {
                        return false;
                    }

                    size_t first = actualPass / ((n - 1) * (n - 2));
                    size_t second = actualPass % ((n - 1) * (n - 2)) / (n - 2);
                    size_t third = actualPass % (n - 2);

                    if (second >= first) {
                        second++;
                    }

                    if (third >= first) {
                        third++;
                    }

                    if (third >= second) {
                        third++;
                    }

                    bool success = do_match(*current.operands[first], *pattern.operands[0], match)
                               && do_match(*current.operands[second], *pattern.operands[1], match)
                               && do_match(*current.operands[third], *pattern.operands[2], match);

                    if (success) {
                        match.special_wildcards[restId] = util::create_rest(current, {first, second, third});
                        return true;
                    }

                    return false;
                }

                default:
                    return false;
            }
        }

        switch (pattern.operands.size()) {
        case 1:
            switch (passIndex) {
                case 0:
                    return do_match(*current.operands[0], *pattern.operands[0], match);
                default:
                    return false;
            }

        case 2:
            switch (passIndex) {
                case 0:
                    return do_match(*current.operands[0], *pattern.operands[0], match)
                           && do_match(*current.operands[1], *pattern.operands[1], match);
                case 1:
                    return do_match(*current.operands[0], *pattern.operands[1], match)
                            && do_match(*current.operands[1], *pattern.operands[0], match);
                default:
                    return false;
            }

        case 3:
            switch (passIndex) {
                case 0:
                    return do_match(*current.operands[0], *pattern.operands[0], match)
                            && do_match(*current.operands[1], *pattern.operands[1], match)
                            && do_match(*current.operands[2], *pattern.operands[2], match);
                case 1:
                    return do_match(*current.operands[0], *pattern.operands[0], match)
                            && do_match(*current.operands[1], *pattern.operands[2], match)
                            && do_match(*current.operands[2], *pattern.operands[1], match);
                case 2:
                    return do_match(*current.operands[0], *pattern.operands[1], match)
                            && do_match(*current.operands[1], *pattern.operands[0], match)
                            && do_match(*current.operands[2], *pattern.operands[2], match);
                case 3:
                    return do_match(*current.operands[0], *pattern.operands[1], match)
                            && do_match(*current.operands[1], *pattern.operands[2], match)
                            && do_match(*current.operands[2], *pattern.operands[0], match);
                case 4:
                    return do_match(*current.operands[0], *pattern.operands[2], match)
                            && do_match(*current.operands[1], *pattern.operands[0], match)
                            && do_match(*current.operands[2], *pattern.operands[1], match);
                case 5:
                    return do_match(*current.operands[0], *pattern.operands[2], match)
                            && do_match(*current.operands[1], *pattern.operands[1], match)
                            && do_match(*current.operands[2], *pattern.operands[0], match);
                default:
                    return false;

            }

        default:
            return false;
        }
    }

    bool is_exhausted() {
        size_t first = 0;

        while (first < indices.size() && indices[first] == totals[first] - 1) {
                first++;
        }

        if (first == indices.size()) {
            return true;
        }

        for (size_t i = 0; i < first; i++) {
            indices[i] = 0;
        }

        indices[first]++;

        return false;
    }

    int find(ExpressionRef nary) {
        for (size_t i = 0; i < narys.size(); i++) {
            if (&narys[i].get() == &nary) {
                return (int) i;
            }
        }

        return -1;
    }

    size_t factorial(size_t n) {
        size_t ret = 1;
        while (n > 0) {
            ret *= (n--);
        }

        return ret;
    }
};

#endif
