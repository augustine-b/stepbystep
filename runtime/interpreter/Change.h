#ifndef CHANGE_H
#define CHANGE_H

#include "../tree/ExpressionNode.h"

struct Change
{
    ExpressionRef from;

    std::vector<ExpressionPtr> to;

    Change(ExpressionRef from, std::vector<ExpressionPtr>&& to)
        : from(from), to(std::move(to)) {}
};

#endif
