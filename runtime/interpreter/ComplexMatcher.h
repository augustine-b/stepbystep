#ifndef STEPBYSTEP_COMPLEXMATCHER_H
#define STEPBYSTEP_COMPLEXMATCHER_H

#include <deque>
#include <optional>
#include "../tree/ExpressionNode.h"
#include "../tree/OperatorNode.h"
#include "../tree/WildcardNode.h"
#include "../tree/SpecialWildcardNode.h"
#include "../utility/Functions.h"

struct ComplexMatch
{
    ExpressionRef completeMatch;

    std::array<ExpressionAddr, 10> wildcards;

    std::array<ExpressionPtr, 1> special_wildcards;

    explicit ComplexMatch(ExpressionRef completeMatch)
            : completeMatch {completeMatch}, wildcards {}, special_wildcards{} {}

    void null() {
        for (int i = 0; i < 10; i++) {
            wildcards[i] = nullptr;
        }
    }
};

class ComplexMatcher
{
    ExpressionPtr pattern;

    std::reference_wrapper<const ExpressionNode> current;

    std::deque<std::reference_wrapper<const ExpressionNode>> queue;

    size_t index;

    size_t total;

public:

    std::array<bool(*)(ExpressionRef), 10> constraints;

    ComplexMatcher(ExpressionPtr pattern, ExpressionRef base)
            : pattern {std::move(pattern)}, current {base}, queue {}, index {}, total {}, constraints {} {
        queue.emplace_back(base);
    };

    std::optional<ComplexMatch> next_match() {
        while (index < total) {
            ComplexMatch match { current };
            if (do_match(current, *pattern, match)) {
                return { std::move(match) };
            }
        }

        while (!queue.empty()) {
            current = queue[0];
            queue.pop_front();

            if (current.get().is_operation()) {
                for (auto const& op : static_cast<const OperatorNode&>(current.get()).operands) {
                    queue.emplace_back(*op);
                }
            }

            ComplexMatch match {current};

            index = 0;
            total = 0;

            if (do_match(current, *pattern, match)) {
                return { std::move(match) };
            } else {
                while (index < total) {
                    match.null();
                    if (do_match(current, *pattern, match)) {
                        return { std::move(match) };
                    }
                }
            }
        }

        return std::nullopt;
    }

private:

    bool do_match(ExpressionRef current_b, ExpressionRef current_p, ComplexMatch& match) {
        if (current_b.is_operation() && current_p.is_operation()) {
            const auto& operator_b = static_cast<const OperatorNode&>(current_b);
            const auto& operator_p = static_cast<const OperatorNode&>(current_p);

            if (operator_b.op == operator_p.op) {
                if (operator_b.op == Operator::PLUS || operator_b.op == Operator::MULTIPLY) {
                    return do_match_plus_multiply(operator_b, operator_p, match);
                } else if (operator_b.operands.size() == operator_p.operands.size()) {
                    for (size_t i = 0; i < operator_b.operands.size(); i++) {
                        if (!do_match(*operator_b.operands[i], *operator_p.operands[i], match)) {
                            return false;
                        }
                    }

                    return true;
                }
            }

            return false;
        }

        if (current_p.is_wildcard()) {
            const auto& wildcard_p = static_cast<const WildcardNode&>(current_p);

            if (match.wildcards[wildcard_p.id]) {
                return *match.wildcards[wildcard_p.id] == current_b;
            } else {
                if (constraints[wildcard_p.id] && !constraints[wildcard_p.id](current_b)) {
                    return false;
                }

                match.wildcards[wildcard_p.id] = &current_b;
                return true;
            }
        }

        return current_b == current_p;
    }

    bool do_match_plus_multiply(const OperatorNode& current, const OperatorNode& pattern, ComplexMatch& match) {
        bool hasRestCard = pattern.operands.back()->is_special_wildcard();

        if (!hasRestCard && pattern.operands.size() != current.operands.size()) {
            return false;
        }

        if (hasRestCard) {
            size_t n = current.operands.size();
            size_t k = pattern.operands.size() - 1;

            if (k > n) {
                return false;
            }

            if (total == 0) {
                total = variation(n, k);
                index = 0;
            }

            switch (k) {
                case 1: {
                    if (index == n) {
                        return false;
                    }

                    if (!do_match(*current.operands[index], *pattern.operands[0], match)) {
                        index++;
                        return false;
                    }

                    match.special_wildcards[0] = util::create_rest(current, {index});
                    index++;
                    return true;
                }

                case 2: {
                    if (index == n * (n - 1)) {
                        return false;
                    }

                    size_t first = index / (n - 1);
                    size_t second = index % (n - 1);

                    if (second >= first) {
                        second++;
                    }

                    if (!do_match(*current.operands[first], *pattern.operands[0], match)) {
                        index += n - 1;
                        return false;
                    }

                    if (!do_match(*current.operands[second], *pattern.operands[1], match)) {
                        index++;
                        return false;
                    }

                    match.special_wildcards[0] = util::create_rest(current, {first, second});
                    index++;
                    return true;
                }

                case 3: {
                    if (index == n * (n - 1) * (n - 2)) {
                        return false;
                    }

                    size_t first = index / ((n - 1) * (n - 2));
                    size_t second = index % ((n - 1) * (n - 2)) / (n - 2);
                    size_t third = index % (n - 2);

                    if (second >= first) {
                        second++;
                    }

                    if (third >= first) {
                        third++;
                    }

                    if (third >= second) {
                        third++;
                    }

                    if (!do_match(*current.operands[first], *pattern.operands[0], match)) {
                        index += (n - 1) * (n - 2);
                        return false;
                    }

                    if (!do_match(*current.operands[second], *pattern.operands[1], match)) {
                        index += n - 2;
                        return false;
                    }

                    if (!do_match(*current.operands[third], *pattern.operands[2], match)) {
                        index++;
                        return false;
                    }

                    match.special_wildcards[0] = util::create_rest(current, {first, second, third});
                    index++;
                    return false;
                }

                default:
                    return false;
            }
        }

        if (total == 0) {
            total = variation(current.operands.size(), 0);
            index = 0;
        } else {
            index++;
        }

        switch (pattern.operands.size()) {
            case 1:
                switch (index) {
                    case 0:
                        return do_match(*current.operands[0], *pattern.operands[0], match);
                    default:
                        return false;
                }

            case 2:
                switch (index) {
                    case 0:
                        return do_match(*current.operands[0], *pattern.operands[0], match)
                               && do_match(*current.operands[1], *pattern.operands[1], match);
                    case 1:
                        return do_match(*current.operands[0], *pattern.operands[1], match)
                               && do_match(*current.operands[1], *pattern.operands[0], match);
                    default:
                        return false;
                }

            case 3:
                switch (index) {
                    case 0:
                        return do_match(*current.operands[0], *pattern.operands[0], match)
                               && do_match(*current.operands[1], *pattern.operands[1], match)
                               && do_match(*current.operands[2], *pattern.operands[2], match);
                    case 1:
                        return do_match(*current.operands[0], *pattern.operands[0], match)
                               && do_match(*current.operands[1], *pattern.operands[2], match)
                               && do_match(*current.operands[2], *pattern.operands[1], match);
                    case 2:
                        return do_match(*current.operands[0], *pattern.operands[1], match)
                               && do_match(*current.operands[1], *pattern.operands[0], match)
                               && do_match(*current.operands[2], *pattern.operands[2], match);
                    case 3:
                        return do_match(*current.operands[0], *pattern.operands[1], match)
                               && do_match(*current.operands[1], *pattern.operands[2], match)
                               && do_match(*current.operands[2], *pattern.operands[0], match);
                    case 4:
                        return do_match(*current.operands[0], *pattern.operands[2], match)
                               && do_match(*current.operands[1], *pattern.operands[0], match)
                               && do_match(*current.operands[2], *pattern.operands[1], match);
                    case 5:
                        return do_match(*current.operands[0], *pattern.operands[2], match)
                               && do_match(*current.operands[1], *pattern.operands[1], match)
                               && do_match(*current.operands[2], *pattern.operands[0], match);
                    default:
                        return false;

                }

            default:
                return false;
        }
    }

    size_t variation(size_t n, size_t k) {
        k = n - k;

        size_t ret = 1;
        while (n > k) {
            ret *= (n--);
        }

        return ret;
    }
};

#endif
