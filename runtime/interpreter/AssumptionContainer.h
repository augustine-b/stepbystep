#ifndef STEPBYSTEP_ASSUMPTIONCONTAINER_H
#define STEPBYSTEP_ASSUMPTIONCONTAINER_H

#include <vector>

class AssumptionContainer
{
public:

    std::vector<std::tuple<ExpressionPtr, int>> sgn_assumptions;

    std::vector<std::tuple<ExpressionPtr, ExpressionPtr, int>> compare_assumptions;

    std::vector<std::tuple<ExpressionPtr, bool>> integer_assumptions;

};

#endif
