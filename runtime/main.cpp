#include <iostream>
#include <fstream>
#include "tree/ExpressionNode.h"
#include "tree/OperatorNode.h"
#include "tree/ConstantNode.h"
#include "tree/WildcardNode.h"
#include "interpreter/GeneralMatcher.h"
#include "interpreter/Change.h"
#include "interpreter/AssumptionContainer.h"
#include "parser/Driver.h"
#include "utility/ImmutableBitset.h"

#include "../compiled/test_rules.hpp"
#include "../compiled/sgn_heuristic.hpp"
#include "../compiled/compare_heuristic.hpp"

void solve(ExpressionPtr expr, int indent) {
    bool changed;
    size_t last_rule = 0;

    for (int i = 0; i < indent; i++) {
        std::cout << '\t';
    }
    std::cout << expr->to_string() << std::endl;

    do {
        changed = false;

        for (size_t i = 0; i < std::size(rulesets::test::rules); i++) {
            if (rulesets::test::skipifs[i][last_rule]) {
                continue;
            }

            try {
                auto change = rulesets::test::rules[i](*expr);

                if (change) {
                    if (change->to.size() == 1) {
                        auto [replaced, _] = expr->exact_replace(change->from, std::move(change->to[0]));
                        for (int k = 0; k < indent; k++) {
                            std::cout << '\t';
                        }
                        std::cout << replaced->to_string() << std::endl;
                        expr = std::move(replaced);
                        changed = true;
                        last_rule = i;

                        goto outer;
                    } else {
                        for (size_t j = 0; j < change->to.size(); j++) {
                            auto [replaced, _] = expr->exact_replace(change->from, std::move(change->to[j]));
                            solve(std::move(replaced), indent + 1);
                        }
                    }
                }
            } catch (const char* e) {
                std::cerr << e;
                return;
            }
        }

        outer:;
    } while(changed);
}

int main ()
{
    Driver drv;

    std::string base_str;

    std::getline(std::cin, base_str);

    solve(drv.parse_expression(base_str), 0);

    return 0;
}
