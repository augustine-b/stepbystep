#include <utility>

#ifndef VARIABLENODE_H
#define VARIABLENODE_H

#include "ExpressionNode.h"

class VariableNode : public ExpressionNode
{
    const std::string name;

public:

    explicit VariableNode(std::string name) : name(std::move(name)) {};

    ExpressionPtr clone() const override {
        return ExpressionPtr {new VariableNode {name}};
    }

    std::string to_string() const override {
        return std::string(name);
    }

    NodeType get_type() const override {
        return NodeType::VARIABLE;
    }

    bool operator==(const VariableNode& other) const {
        return name == other.name;
    }
};

#endif
