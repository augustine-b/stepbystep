#ifndef WILDCARDNODE_H
#define WILDCARDNODE_H

#include "ExpressionNode.h"

class WildcardNode : public ExpressionNode
{
public:

    int id;

    explicit WildcardNode(int id) : id(id) {};

    ExpressionPtr clone() const override {
        return ExpressionPtr(new WildcardNode(id));
    }

    std::string to_string() const override {
        return "%" + std::to_string(id);
    }

    NodeType get_type() const override {
        return NodeType::WILDCARD;
    }

    bool operator==(const WildcardNode& other) const {
        return id == other.id;
    }
};

#endif
