#ifndef CONSTANTNODE_H
#define CONSTANTNODE_H

#include "../../common/Operator.h"
#include "ExpressionNode.h"
#include "OperatorNode.h"

class ConstantNode : public ExpressionNode
{
    double val;

public:

    explicit ConstantNode(double val) : val(val) {
        if (val < 0) {
            throw std::invalid_argument {"Constant nodes with negative values are not allowed"};
        }
    };

    ConstantNode(const ConstantNode& cn) : val(cn.val) {};

    static ExpressionPtr make(double val) {
        if (val >= 0) {
            return std::make_unique<ConstantNode>(val);
        } else {
            return std::make_unique<OperatorNode>(Operator::MINUS, std::make_unique<ConstantNode>(-val));
        }
    }

    ExpressionPtr clone() const override {
        return ExpressionPtr(new ConstantNode(*this));
    }

    std::string to_string() const override {
        if (is_integer()) {
            return (std::to_string(static_cast<long>(val)));
        }
        
        return std::to_string(val);
    }

    NodeType get_type() const override {
        return NodeType::CONSTANT;
    }

    double get_value() const override {
        return val;
    }

    bool is_integer() const override {
        return static_cast<long>(val) == val;
    }

    bool is_numeric() const override {
        return true;
    }

    bool is_constant() const override {
        return true;
    }

    bool is_even() const override {
        auto rounded = static_cast<long>(val);
        return rounded == val && rounded % 2 == 0;
    }

    bool operator==(const ConstantNode& other) const {
        return val == other.val;
    }
};

#endif
