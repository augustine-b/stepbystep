#ifndef EXPRESSIONNODE_H
#define EXPRESSIONNODE_H

#include <string>
#include <vector>
#include <memory>
#include <optional>
#include <utility>
#include <iostream>
#include "../../common/Operator.h"

class OperatorNode;
class WildcardNode;
class ExpressionNode;

using ExpressionPtr = std::unique_ptr<ExpressionNode>;

using ExpressionRef = const ExpressionNode&;

using ExpressionAddr = const ExpressionNode*;

enum class NodeType {
    NULLNODE,
    CONSTANT,
    VARIABLE,
    OPERATOR,
    WILDCARD,
    SWILDCARD,
};

class ExpressionNode
{
public:

    virtual ~ExpressionNode() = default;

    virtual ExpressionPtr clone() const = 0;

    virtual std::string to_string() const = 0;

    virtual NodeType get_type() const = 0;

    bool is_operation() const { return get_type() == NodeType::OPERATOR; }

    virtual bool is_operation(const Operator&) const { return false; }

    bool is_negative() const { return is_operation(Operator::MINUS); }

    bool is_wildcard() const { return get_type() == NodeType::WILDCARD; }

    bool is_special_wildcard() const { return get_type() == NodeType::SWILDCARD; }

    virtual std::pair<ExpressionPtr, ExpressionPtr> exact_replace(ExpressionRef from, ExpressionPtr to) const {
        if (&from == this) {
            return {std::move(to), nullptr};
        }

        return {clone(), std::move(to)};
    }

    virtual bool is_integer() const { return false; }

    virtual bool is_numeric() const { return false; }

    virtual bool is_constant() const { return false; }

    virtual bool is_even() const { return false; }

    virtual double get_value() const { return std::numeric_limits<double>::quiet_NaN(); };

    static std::vector<ExpressionPtr> copy(const std::vector<ExpressionPtr>& vector) {
        std::vector<ExpressionPtr> copy(vector.size());

        for (size_t i = 0; i < vector.size(); i++) {
            copy[i] = vector[i]->clone();
        }

        return copy;
    }
};

class NullNode : public ExpressionNode
{
public:

    ExpressionPtr clone() const override {
        return ExpressionPtr(new NullNode());
    }

    std::string to_string() const override {
        return "NULL";
    }

    NodeType get_type() const override {
        return NodeType::NULLNODE;
    }

    bool operator==(const NullNode&) const {
        return true;
    }
};


#endif
