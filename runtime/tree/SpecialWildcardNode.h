#ifndef STEPBYSTEP_SPECIALWILDCARDNODE_H
#define STEPBYSTEP_SPECIALWILDCARDNODE_H

#include "ExpressionNode.h"

class SpecialWildcardNode : public ExpressionNode
{
public:

    int id;

    explicit SpecialWildcardNode(int id) : id(id) {};

    ExpressionPtr clone() const override {
        return ExpressionPtr(new SpecialWildcardNode(id));
    }

    std::string to_string() const override {
        return "%%" + std::to_string(id);
    }

    NodeType get_type() const override {
        return NodeType::SWILDCARD;
    }

    bool operator==(const SpecialWildcardNode& other) const {
        return id == other.id;
    }
};

#endif
