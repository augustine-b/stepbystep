#include <utility>

#ifndef OPERATORNODE_H
#define OPERATORNODE_H

#include <vector>
#include <sstream>
#include <iostream>
#include <cmath>
#include "ExpressionNode.h"
#include "../../common/Operator.h"
#include "../utility/Operators.h"

class OperatorNode : public ExpressionNode
{
public:

    Operator op;

    std::vector<ExpressionPtr> operands;

    OperatorNode(Operator op, ExpressionPtr operand)
            : op(std::move(op)), operands() {
        operands.push_back(std::move(operand));
    }

    OperatorNode(Operator op, ExpressionPtr operand1, ExpressionPtr operand2)
            : op(std::move(op)), operands() {
        if (op == Operator::PLUS || op == Operator::MULTIPLY) {
            if (operand1->is_operation(op)) {
                auto& on = static_cast<OperatorNode&>(*operand1);

                for (auto& operand : on.operands) {
                    operands.push_back(std::move(operand));
                }
            } else {
                operands.push_back(std::move(operand1));
            }

            if (operand2->is_operation(op)) {
                auto& on = static_cast<OperatorNode&>(*operand2);

                for (auto& operand : on.operands) {
                    operands.push_back(std::move(operand));
                }
            } else {
                operands.push_back(std::move(operand2));
            }
        } else {
            operands.push_back(std::move(operand1));
            operands.push_back(std::move(operand2));
        }
    }

    OperatorNode(Operator op, std::vector<ExpressionPtr>&& operands)
            : op(std::move(op)), operands(build_operands(std::move(operands))) {}

    OperatorNode(const OperatorNode& on)
            : op(on.op), operands(ExpressionNode::copy(on.operands)) {}

    NodeType get_type() const override {
        return NodeType::OPERATOR;
    }

    bool operator==(const OperatorNode& other) const {
        if (op != other.op || operands.size() != other.operands.size()) {
            return false;
        }

        if (op != Operator::PLUS && op != Operator::MULTIPLY) {
            for (size_t i = 0; i < operands.size(); i++) {
                if (*operands[i] != *other.operands[i]) {
                    return false;
                }
            }

            return true;
        }

        switch (operands.size()) {
        case 1:
            return *operands[0] == *other.operands[0];

        case 2:
            return (*operands[0] == *other.operands[0] && *operands[1] == *other.operands[1])
                || (*operands[0] == *other.operands[1] && *operands[1] == *other.operands[0]);
        case 3:
            if (*operands[0] == *other.operands[0]) {
                return (*operands[1] == *other.operands[1] && *operands[2] == *other.operands[2])
                    || (*operands[1] == *other.operands[2] && *operands[2] == *other.operands[1]);
            } else if (*operands[0] == *other.operands[1]) {
                return (*operands[1] == *other.operands[0] && *operands[2] == *other.operands[2])
                    || (*operands[1] == *other.operands[2] && *operands[2] == *other.operands[0]);
            } else if (*operands[0] == *other.operands[2]) {
                return (*operands[1] == *other.operands[0] && *operands[2] == *other.operands[1])
                    || (*operands[1] == *other.operands[1] && *operands[2] == *other.operands[0]);
            }

            return false;

        default:
            return false;
        }
    }

    ExpressionPtr clone() const override {
        return ExpressionPtr(new OperatorNode(*this));
    }

    std::string to_string() const override {
        if (op.arity == 1) {
            bool is_operator = dynamic_cast<OperatorNode*>(operands[0].get());

            if (op.function || is_operator) {
                return op.str + "(" + operands[0]->to_string() + ")";
            } else {
                return op.str + operands[0]->to_string();
            }
        }

        if (op == Operator::EQUALS) {
            return operands[0]->to_string() + " = " + operands[1]->to_string();
        }

        std::string separator;

        if (op.function) {
            separator = ", ";
        } else {
            separator = " " + op.str + " ";
        }

        std::string str;

        if (op == Operator::PLUS) {
            for (auto& en : operands) {
                if (str.empty()) {
                    str += en->to_string();
                } else if (en->is_negative()) {
                    auto on = static_cast<const OperatorNode&>(*en);
                    str += " - " + on.operands[0]->to_string();
                } else {
                    str += " + " + en->to_string();
                }
            }
        } else {
            for (const auto& en : operands) {
                if (!str.empty()) {
                    str += separator;
                }

                int priority = -1;

                if (en->is_operation()) {
                    priority = static_cast<const OperatorNode&>(*en).op.priority;
                }

                if (priority != -1 && priority <= op.priority) {
                    str += "(";
                }

                str += en->to_string();

                if (priority != -1 && priority <= op.priority) {
                    str += ")";
                }
            }
        }

        if (op.function) {
            return op.str + "(" + str + ")";
        }

        return str;
    }

    bool is_operation(const Operator& op) const override {
        return this->op == op;

    }

    bool is_integer() const override {
        return op == Operator::MINUS && !operands[0]->is_operation() && operands[0]->is_integer();
    }

    bool is_numeric() const override {
        return op == Operator::MINUS && !operands[0]->is_operation() && operands[0]->is_numeric();
    }

    bool is_constant() const override {
        for (const auto& operand : operands) {
            if (!operand->is_constant()) {
                return false;
            }
        }

        return true;
    }

    bool is_even() const override {
        return op == Operator::MINUS && !operands[0]->is_operation() && operands[0]->is_even();
    }

    double get_value() const override {
        if (op == Operator::PLUS) {
            double sum = 0;
            for (auto& en : operands) {
                sum += en->get_value();
            }
            return sum;
        } else if (op == Operator::MINUS) {
            return -operands[0]->get_value();
        } else if (op == Operator::MULTIPLY) {
            double prod = 1;
            for (auto& en : operands) {
                prod *= en->get_value();
            }
            return prod;
        } else if (op == Operator::DIVIDE) {
            return operands[0]->get_value() / operands[1]->get_value();
        } else if (op == Operator::POWER) {
            return std::pow(operands[0]->get_value(), operands[1]->get_value());
        } else if (op == Operator::NROOT) {
            return std::pow(operands[0]->get_value(), 1 / operands[1]->get_value());
        } else {
            std::cerr << "Unhandled operator in get_value: " << op.str << std::endl;
            return 0;
        }
    }

    std::pair<ExpressionPtr, ExpressionPtr> exact_replace(ExpressionRef from, ExpressionPtr to) const override {
        if (&from == this) {
            return {std::move(to), nullptr};
        }

        bool found = false;
        std::vector<ExpressionPtr> temp(operands.size());

        for (size_t i = 0; i < operands.size(); i++) {

            auto [replaced, ptr] = operands[i]->exact_replace(from, std::move(to));

            if (ptr == nullptr) {
                if (!found) {
                    for (size_t j = 0; j < i; j++) {
                        temp[j] = operands[j]->clone();
                    }

                    found = true;
                }

                temp[i] = std::move(replaced);
            } else if (found) {
                temp[i] = operands[i]->clone();
            } else {
                to = std::move(ptr);
            }
        }

        if (found) {
            return {ExpressionPtr(new OperatorNode(op, std::move(temp))), nullptr};
        }

        return {clone(), std::move(to)};
    }

private:

    std::vector<ExpressionPtr> build_operands(std::vector<ExpressionPtr>&& operands) {
        if (op == Operator::PLUS || op == Operator::MULTIPLY) {
            std::vector<ExpressionPtr> temp;

            for (auto& en : operands) {
                if (en->is_operation(op)) {
                    auto on = static_cast<OperatorNode&>(*en);

                    for (auto& operand : on.operands) {
                        temp.push_back(std::move(operand));
                    }
                } else {
                    temp.push_back(std::move(en));
                }
            }

            return temp;
        } else {
            return std::move(operands);
        }
    }
};

#endif



