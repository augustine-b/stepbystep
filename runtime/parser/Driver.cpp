#include "Driver.h"
#include "../generated/Parser.hpp"

ExpressionPtr Driver::parse_expression(const std::string &str) {
    this->mode = 0;

    begin_string(str);

    ExpressionPtr exp;
    yy::parser parse(*this, exp);
    err = parse();

    return exp;
}

ExpressionPtr Driver::parse_pattern(const std::string &str) {
    this->mode = 1;

    begin_string(str);

    ExpressionPtr exp;
    yy::parser parse(*this, exp);
    err = parse();

    return exp;
}
