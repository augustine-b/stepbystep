#ifndef DRIVER_H
#define DRIVER_H

#include <string>
#include <map>
#include "../generated/Parser.hpp"
#include "../tree/ExpressionNode.h"

# define YY_DECL \
  yy::parser::symbol_type yylex ()

YY_DECL;

class Driver
{
public:

    int err;

    int mode;

    Driver()
      : err(), mode() {};

    ExpressionPtr parse_expression(const std::string& str);

    ExpressionPtr parse_pattern(const std::string& str);

    void begin_string(const std::string& str);
    void begin_file(const std::string& file);
};

#endif
