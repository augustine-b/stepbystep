#ifndef OPERATORS_H
#define OPERATORS_H

#include "../tree/ExpressionNode.h"

bool operator==(ExpressionRef a, ExpressionRef b);

bool operator!=(ExpressionRef a, ExpressionRef b);

bool operator<(ExpressionRef a, ExpressionRef b);

bool operator<=(ExpressionRef a, ExpressionRef b);

bool operator>(ExpressionRef a, ExpressionRef b);

bool operator>=(ExpressionRef a, ExpressionRef b);

ExpressionPtr operator+(ExpressionPtr a, ExpressionPtr b);

ExpressionPtr operator-(ExpressionPtr a);

ExpressionPtr operator*(ExpressionPtr a, ExpressionPtr b);

ExpressionPtr operator/(ExpressionPtr a, ExpressionPtr b);

ExpressionPtr operator^(ExpressionPtr a, ExpressionPtr b);

ExpressionPtr eq(ExpressionPtr a, ExpressionPtr b);

ExpressionPtr nroot(ExpressionPtr a, ExpressionPtr b);

ExpressionPtr log(ExpressionPtr a, ExpressionPtr b);

ExpressionPtr abs(ExpressionPtr a);

ExpressionPtr plusminus(ExpressionPtr a);

#endif
