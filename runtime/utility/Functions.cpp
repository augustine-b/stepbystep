#include <numeric>
#include "Functions.h"
#include "../tree/ConstantNode.h"

ExpressionPtr util::eval(ExpressionRef x) {
    return ConstantNode::make(x.get_value());
}

ExpressionPtr util::gcd(const ExpressionNode &a, const ExpressionNode &b) {
    return ConstantNode::make(std::gcd(static_cast<long>(a.get_value()), static_cast<long>(b.get_value())));
}

int util::round(const ExpressionNode &a) {
    return static_cast<long>(a.get_value());
}

std::pair<ExpressionPtr, ExpressionPtr> util::split_integer(const ExpressionNode& x) {
    bool negative {false};

    ExpressionRef tmp = [&]() -> ExpressionRef {
        if (x.is_negative()) {
            negative = true;
            return *static_cast<const OperatorNode&>(x).operands[0];
        }

        return x;
    }();

    if (tmp.is_operation(Operator::MULTIPLY)) {
        ExpressionPtr integer {nullptr};
        std::vector<ExpressionPtr> remainder {};

        auto op = static_cast<const OperatorNode&>(tmp);

        for (const auto& operand : op.operands) {
            if (!integer && operand->is_integer()) {
                integer = operand->clone();
            } else {
                remainder.push_back(operand->clone());
            }
        }

        if (negative) {
            integer = -std::move(integer);
        }

        if (remainder.size() == 1) {
            return { std::move(integer), std::move(remainder[0]) };
        }

        return { std::move(integer), std::make_unique<OperatorNode>(Operator::MULTIPLY, std::move(remainder)) };
    }

    if (negative) {
        return { ConstantNode::make(-1), tmp.clone() };
    }

    return { ConstantNode::make(1), tmp.clone() };
}

ExpressionPtr util::create_rest(const OperatorNode& current, const std::vector<size_t>& except) {
    std::vector<ExpressionPtr> operands;

    for (size_t i = 0; i < current.operands.size(); i++) {
        if (!contains(except, i)) {
            operands.push_back(current.operands[i]->clone());
        }
    }

    if (operands.empty()) {
        return std::make_unique<NullNode>();
    }

    if (operands.size() == 1) {
        return operands[0]->clone();
    }

    return std::make_unique<OperatorNode>(current.op, std::move(operands));
}

bool util::contains(const std::vector<size_t>& except, size_t current) {
    for (size_t i : except) {
        if (i == current) {
            return true;
        }
    }

    return false;
}

std::string util::to_string(const ExpressionNode& expression) {
    return expression.to_string();
}

std::string util::to_string(double val) {
    return std::to_string(val);
}

std::string util::to_string(int val) {
    return std::to_string(val);
}
