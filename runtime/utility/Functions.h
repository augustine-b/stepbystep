#ifndef UTILITY_H
#define UTILITY_H

#include "../tree/ExpressionNode.h"

namespace util
{
    ExpressionPtr eval(ExpressionRef x);

    ExpressionPtr gcd(ExpressionRef a, ExpressionRef b);

    int round(const ExpressionNode& a);

    std::pair<ExpressionPtr, ExpressionPtr> split_integer(ExpressionRef x);

    ExpressionPtr create_rest(const OperatorNode& current, const std::vector<size_t>& except);

    bool contains(const std::vector<size_t>& except, size_t current);

    std::string to_string(ExpressionRef expression);

    std::string to_string(double val);

    std::string to_string(int val);
}

#endif
