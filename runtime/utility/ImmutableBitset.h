#ifndef STEPBYSTEP_IMMUTABLEBITSET_H
#define STEPBYSTEP_IMMUTABLEBITSET_H

#include <string_view>

template <int Size>
class immutable_bitset
{
    int8_t _bits[(Size + 7) / 8];

public:

    constexpr immutable_bitset() = default;

    constexpr explicit immutable_bitset(std::string_view init)
            : _bits {}
    {
        for (size_t i = 0; i < init.size() && i < Size; i++) {
            _bits[i / 8] |= (init[i] == '1') << (7 - i % 8);
        }
    }

    constexpr size_t size() const {
        return Size;
    }

    constexpr bool operator[](size_t pos) const {
        return _bits[pos / 8] & (0b10000000u >> (pos % 8));
    }
};

template <size_t N>
immutable_bitset(const char (&array)[N]) -> immutable_bitset<N - 1>;

#endif
