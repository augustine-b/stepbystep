#include "../tree/ExpressionNode.h"
#include "../tree/ConstantNode.h"
#include "../tree/VariableNode.h"
#include "../tree/WildcardNode.h"
#include "../../common/Operator.h"
#include "Operators.h"


bool operator==(ExpressionRef a, ExpressionRef b) {
    NodeType type = a.get_type();

    if (type != b.get_type()) {
        return false;
    }

    switch (type) {
        case NodeType::CONSTANT:
            return static_cast<const ConstantNode&>(a) == static_cast<const ConstantNode&>(b);
        case NodeType::VARIABLE:
            return static_cast<const VariableNode&>(a) == static_cast<const VariableNode&>(b);
        case NodeType::OPERATOR:
            return static_cast<const OperatorNode&>(a) == static_cast<const OperatorNode&>(b);
        case NodeType::WILDCARD:
            return static_cast<const WildcardNode&>(a) == static_cast<const WildcardNode&>(b);
        default:
            return false;
    }
}

bool operator!=(const ExpressionNode &a, const ExpressionNode &b) {
    return !(a == b);
}

bool operator<(const ExpressionNode &a, const ExpressionNode &b) {
    return a.get_value() < b.get_value();
}

bool operator<=(const ExpressionNode &a, const ExpressionNode &b) {
    return a.get_value() <= b.get_value();
}

bool operator>(const ExpressionNode &a, const ExpressionNode &b) {
    return a.get_value() > b.get_value();
}

bool operator>=(const ExpressionNode &a, const ExpressionNode &b) {
    return a.get_value() >= b.get_value();
}

ExpressionPtr operator+(ExpressionPtr a, ExpressionPtr b) {
    if (a->get_type() == NodeType::NULLNODE) {
        return b;
    }

    if (b->get_type() == NodeType::NULLNODE) {
        return a;
    }

    return std::make_unique<OperatorNode>(Operator::PLUS, std::move(a), std::move(b));
}

ExpressionPtr operator-(ExpressionPtr a) {
    return std::make_unique<OperatorNode>(Operator::MINUS, std::move(a));
}

ExpressionPtr operator*(ExpressionPtr a, ExpressionPtr b) {
    if (a->get_type() == NodeType::NULLNODE) {
        return b;
    }

    if (b->get_type() == NodeType::NULLNODE) {
        return a;
    }

    return std::make_unique<OperatorNode>(Operator::MULTIPLY, std::move(a), std::move(b));
}

ExpressionPtr operator/(ExpressionPtr a, ExpressionPtr b) {
    return std::make_unique<OperatorNode>(Operator::DIVIDE, std::move(a), std::move(b));
}

ExpressionPtr operator^(ExpressionPtr a, ExpressionPtr b) {
    return std::make_unique<OperatorNode>(Operator::POWER, std::move(a), std::move(b));
}

ExpressionPtr eq(ExpressionPtr a, ExpressionPtr b) {
    return std::make_unique<OperatorNode>(Operator::EQUALS, std::move(a), std::move(b));
}

ExpressionPtr nroot(ExpressionPtr a, ExpressionPtr b) {
    return std::make_unique<OperatorNode>(Operator::NROOT, std::move(a), std::move(b));
}

ExpressionPtr log(ExpressionPtr a, ExpressionPtr b) {
    return std::make_unique<OperatorNode>(Operator::LOG, std::move(a), std::move(b));
}

ExpressionPtr abs(ExpressionPtr a) {
    return std::make_unique<OperatorNode>(Operator::ABS, std::move(a));
}

ExpressionPtr plusminus(ExpressionPtr a) {
    return std::make_unique<OperatorNode>(Operator::PLUSMINUS, std::move(a));
}
