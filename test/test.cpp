#include <iostream>
#include <chrono>
#include <fstream>
#include "../runtime/parser/Driver.h"
#include "../runtime/tree/ExpressionNode.h"
#include "../runtime/tree/OperatorNode.h"
#include "../runtime/tree/ConstantNode.h"
#include "../runtime/tree/WildcardNode.h"
#include "../runtime/interpreter/GeneralMatcher.h"
#include "../runtime/interpreter/AssumptionContainer.h"
#include "../compiled/sgn_heuristic.hpp"
#include "../compiled/compare_heuristic.hpp"

using namespace std;
using namespace std::chrono;

Driver drv;

bool check_match(const std::vector<std::string>& expected, const GeneralMatch& actual) {
    for (size_t i = 0; i < 10; i++) {
        if (i < expected.size()) {
            if (*drv.parse_expression(expected[i]) != *actual.wildcards[i]) {
                return false;
            }
        } else {
            if (actual.wildcards[i] != nullptr) {
                return false;
            }
        }
    }

    return true;
}

static int mtIndex;
static int mtSuccess;
static string mtString;

void mt(const string& pattern, const string& exp, const vector<string>& oneMatch, int totalMatches) {
    mtIndex++;

    auto p = drv.parse_pattern(pattern);
    auto e = drv.parse_expression(exp);

    GeneralMatcher m(std::move(p), *e);

    bool found = false;
    int cnt = 0;

    while (auto match = m.next_match()) {
        cnt++;

        if (!found && check_match(oneMatch, *match)) {
            found = true;
        }
    }

    if (!found && totalMatches != 0) {
        cerr << "Error in match test " << mtIndex << ": Could not find specified match" << endl;
    }

    if (cnt != totalMatches) {
        cerr << "Error in match test " << mtIndex << ": Expected " << totalMatches << " matches, but got " << cnt << endl;
    }

    if ((found || totalMatches == 0) && cnt == totalMatches) {
        mtString += "#";
        mtSuccess++;
    } else {
        mtString += "X";
    }
}

void match_test() {
    cout << "Running: Match tests" << endl;

    auto t1 = high_resolution_clock::now();

    mt("(%0 + %1)*(%2 + %3)", "(a+b)*(c+d)", { "a", "b", "c", "d" }, 8);
    mt("(%0 + %1)*(%2 + %3)", "(a+b)*(c+d)", { "b", "a", "d", "c" }, 8);
    mt("(%0 + %1 + %2)*(%0 + %0 + %2)", "(a+b+c)*(b+b+a)", { "b", "c", "a" }, 2);
    mt("%0 + %1 + %%", "1+a+3+b+a", { "1", "3" }, 120);
    mt("%0 + %1 + %2 + %%", "1+a+5+3+b+a", { "1", "a", "5" }, 720);
    mt("%0 + %0 + %1 + %%", "1+a+5+3+b+a", { "a", "b" }, 48);
    mt("%0 + %1 + %%", "2/1+1", {"1", "2/1"}, 2);
    mt("%0 + %1 + %2 + %%", "2/1+1", {}, 0);

    auto t2 = high_resolution_clock::now();

    cout << mtString << endl;
    cout << mtSuccess << " / " << mtIndex << endl;
    cout << duration_cast<microseconds>(t2 - t1).count() << " microseconds" << endl << endl;
}

static int shtIndex;
static int shtSuccess;
static string shtString;

void sht(AssumptionContainer& container, const string& exp, int expected) {
    shtIndex++;

    auto e = drv.parse_expression(exp);

    int actual = heuristic::prove_sgn(container, *e);

    if (actual != expected) {
        cerr << "Error in sgn heuristic test " << shtIndex << " (expression: " << exp << "): Expected "
             << expected << ", but got " << actual << endl;
        shtString += "X";
    } else {
        shtString += "#";
        shtSuccess++;
    }
}

void heuristic_test() {
    AssumptionContainer container;

    cout << "Running: Heuristic tests" << endl;

    auto t1 = high_resolution_clock::now();

    sht(container, "3", 1);
    sht(container, "x+1", 0);
    sht(container, "x^2+1", 1);

    heuristic::assume_sgn(container, std::make_unique<VariableNode>("x"), 1);

    sht(container, "x+1", 1);
    sht(container, "-x-1", -1);
    sht(container, "-x+1", 0);
    sht(container, "x^2+3^x+abs(-y)", 1);

    heuristic::assume_compare(
            container,
            std::make_unique<VariableNode>("w"),
            drv.parse_expression("z^2+3^z"),
            1
    );

    sht(container, "z^2+3^z", 1);
    sht(container, "-w-5", -1);

    auto t2 = high_resolution_clock::now();

    cout << shtString << endl;
    cout << shtSuccess << " / " << shtIndex << endl;
    cout << duration_cast<microseconds>(t2 - t1).count() << " microseconds" << endl << endl;
}

int main (int argc, char *argv[])
{
    match_test();

    heuristic_test();

    return 0;
}
